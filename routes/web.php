<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
Route::get('/pendaftaran-pasien-lama', [App\Http\Controllers\Web\RegLamaController::class, 'index'])->name('reg.lama');
Route::get('/identitas-pasien-lama', [App\Http\Controllers\Web\RegLamaIdentitasController::class, 'index'])->name('reg.lama.identitas');
Route::get('/pendaftaran-pasien-baru', [App\Http\Controllers\Web\RegBaruController::class, 'index'])->name('reg.baru');
Route::get('/pendaftaran-berhasil', [App\Http\Controllers\Web\RegSuksesController::class, 'index'])->name('reg.sukses');
Route::get('/jadwal-dokter', [App\Http\Controllers\Web\JadwalController::class, 'index'])->name('jadwal');
Route::get('/antrean', [App\Http\Controllers\Web\AntreanController::class, 'index'])->name('antrean');
Route::get('/ketersediaan-kamar', [App\Http\Controllers\Web\KamarController::class, 'index'])->name('kamar');
Route::get('/', [App\Http\Controllers\Web\DashboardController::class, 'index'])->name('dashboard');
Route::get('/riwayat-booking', [App\Http\Controllers\Web\RiwBookingController::class, 'index'])->name('riw.booking');
Route::get('/identitas-riwayat-booking', [App\Http\Controllers\Web\RiwBookingIdentitasController::class, 'index'])->name('riw.booking.identitas');
