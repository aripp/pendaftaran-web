<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::get('/ref/kecamatan/kabupaten/{kabupaten?}', [App\Http\Controllers\Api\RefController::class, 'kecamatan']);

Route::get('/jadwal', [App\Http\Controllers\Api\JadwalController::class, 'list']);
Route::get('/jadwal/show-by-poli/{poli?}', [App\Http\Controllers\Api\JadwalController::class, 'showByPoli']);
Route::get('/jadwal/show-by-tanggal/{tanggal?}', [App\Http\Controllers\Api\JadwalController::class, 'showByTanggal']);
Route::get('/jadwal/show-by-id/{id?}/tanggal/{tanggal?}', [App\Http\Controllers\Api\JadwalController::class, 'getByIdAndTanggal']);

Route::get('/antrean/show-by-jadwal/{jadwal?}/tanggal/{tanggal?}', [App\Http\Controllers\Api\AntreanController::class, 'getByJadwalAndTanggal']);
Route::get('/kamar', [App\Http\Controllers\Api\KamarController::class, 'list']);

Route::post('/pendaftaran-save', [App\Http\Controllers\Api\RegController::class, 'save']);
Route::post('/pendaftaran-cek-riwayat', [App\Http\Controllers\Api\RegController::class, 'listByNikTanggalLahir']);
Route::get('/bukti-booking', [App\Http\Controllers\Api\BuktiBookingController::class, 'print']);
Route::post('/cek-pasien', [App\Http\Controllers\Api\PasienController::class, 'show']);
