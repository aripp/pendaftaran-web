<?php

namespace App\Exceptions;

use App\Exceptions\ApiException;
use App\Exceptions\WebException;
use App\Helpers\Facades\ApiHelper;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
        $this->renderable(function (ApiException $e, $request) {
            // if ($request->is('api/*')) {
            return ApiHelper::jsonResponse($e->getCode(), $e->getMessage());
            // }
        });
        $this->renderable(function (WebException $e, $request) {
            return response("Ada masalah silahkan coba lagi beberapa saat. Code " . $e->getCode() . " | Message " . $e->getMessage(), $e->getCode());
        });
    }
}
