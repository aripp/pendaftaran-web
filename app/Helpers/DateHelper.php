<?php

namespace App\Helpers;

use Illuminate\Support\Carbon;
use stdClass;

class DateHelper
{

    public function umur($tglLahir)
    {
        // $tglLahir = Carbon::createFromFormat("d-m-Y", $tglLahir);
        $tglLahir = Carbon::create($tglLahir);
        $tglSekarang = Carbon::now();

        $selisih = $tglLahir->diff($tglSekarang);

        $obj = new stdClass();
        $obj->hari = $selisih->d;
        $obj->bulan = $selisih->m;
        $obj->tahun = $selisih->y;

        return $obj;
    }

    public function namaTypeByUmur($tglLahir, $jenisKelamin, $statusPerkawinan)
    {
        if ($statusPerkawinan == "Belum Kawin") {
            $umur = $this->umur($tglLahir);
            if ($umur->tahun < 1) {
                $namaType = "BY";
            } else {
                $namaType = "SDR";
            }
        } else {
            if ($jenisKelamin == "L") {
                $namaType = "BP";
            } else {
                $namaType = "NY";
            }
        }
        return $namaType;
    }

}
