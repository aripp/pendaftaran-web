<?php

namespace App\Helpers;

use Illuminate\Support\Carbon;

class ApiHelper
{

    public function jsonResponse($resStatusCode, $resMessage, $resData = null)
    {
        $metadata = [
            'message' => $resMessage,
            'status_code' => $resStatusCode,
            'date' => Carbon::now()->toDateTimeString(),
        ];

        $response = [
            'metadata' => $metadata,
            'data' => $resData,
        ];

        return response()->json($response, $resStatusCode);
    }
}
