<?php

namespace App\Helpers\Facades;

use Illuminate\Support\Facades\Facade;

class ApiHelper extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'ApiHelper';
    }
}
