<?php

namespace App\Helpers\Facades;

use Illuminate\Support\Facades\Facade;

class DateHelper extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'DateHelper';
    }
}
