<?php

namespace App\Helpers\Facades;

use Illuminate\Support\Facades\Facade;

class AsuransiHelper extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'AsuransiHelper';
    }
}
