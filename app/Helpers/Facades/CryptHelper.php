<?php

namespace App\Helpers\Facades;

use Illuminate\Support\Facades\Facade;

class CryptHelper extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'CryptHelper';
    }
}
