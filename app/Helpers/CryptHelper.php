<?php

namespace App\Helpers;

class CryptHelper
{

    public function encript($string)
    {
        return openssl_encrypt($string, "AES-128-CTR", "3dp", 0, "1234567891011121");
    }
    public function decript($string)
    {
        return openssl_decrypt($string, "AES-128-CTR", "3dp", 0, "1234567891011121");
    }
}
