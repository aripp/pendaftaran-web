<?php

namespace App\Helpers;

class AsuransiHelper
{

    public function bpjsAsuransiId()
    {
        // mapping asuransi id pada tabel master asuransi.
        return explode(",", env('BPJS_ASURANSI_ID', ""));
    }
}
