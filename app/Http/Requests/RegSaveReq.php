<?php

namespace App\Http\Requests;

use App\Helpers\Facades\ApiHelper;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class RegSaveReq extends FormRequest
{
    // protected $redirectRoute = 'reg.baru.save';
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            "nama" => "required",
            "no_ktp" => "required|digits:16",
            "jenis_kelamin" => "required",
            "tgl_lahir" => "required|date_format:d-m-Y",
            "tmp_lahir" => "required",
            "kota_kd" => "required",
            "kecamatan_kd" => "required",
            "alamat" => "required",
            "agama" => "required",
            "etnis" => "required",
            "bahasa" => "required",
            "pendidikan" => "required",
            "pekerjaan" => "required",
            "status_perkawinan" => "required",
            // "nama_ibu" => "required",
            // "nama_ayah" => "required",
            "no_telp" => "required|numeric",
            // "no_wa" => "numeric",
            // "email" => "email",

            "asuransi_id" => "required",
            "tgl_masuk_only_date" => "required|date_format:d-m-Y",
            "jadwal_id" => "required",
            // 'g-recaptcha-response' => 'required|captcha',
        ];

    }
    public function messages()
    {
        return [
            "nama.required" => 'Nama harus diisi',
            "no_ktp.required" => 'No Ktp harus diisi',
            "no_ktp.digits" => 'No Ktp harus 16 digit',
            "jenis_kelamin.required" => 'Jenis kelamin harus diisi',
            "tgl_lahir.required" => 'Tanggal lahir harus diisi',
            "tgl_lahir.date_format" => 'Format tanggal lahir (dd-mm-yyyy)',
            "tmp_lahir.required" => 'Tempat lahir harus diisi',
            "kota_kd.required" => 'Alamat(kabupaten) harus diisi',
            "kecamatan_kd.required" => 'Alamat(kecamatan) harus diisi',
            "alamat.required" => 'Alamat(lengkap) harus diisi',
            "agama.required" => 'Agama harus diisi',
            "etnis.required" => 'Etnis harus diisi',
            "bahasa.required" => 'Bahasa harus diisi',
            "pendidikan.required" => 'Pendidikan harus diisi',
            "pekerjaan.required" => 'Pekerjaan harus diisi',
            "status_perkawinan.required" => 'Status Perkawinan harus diisi',
            // "nama_ibu.required" => 'Nama Ibu harus diisi',
            // "nama_ayah.required" => 'Nama Ayah harus diisi',
            "no_telp.required" => 'No Telp harus diisi',
            "no_telp.numeric" => 'No Telp harus angka',
            // "no_wa.numeric" => 'No Whatsapp harus angka',
            // "email.email" => 'email tidak valid',

            "asuransi_id.required" => 'Cara bayar/Asuransi harus diisi',
            "tgl_masuk_only_date.required" => 'Tanggal masuk harus diisi',
            "tgl_masuk_only_date.date_format" => 'Format tanggal masuk (dd-mm-yyyy)',
            "jadwal_id.required" => 'Jadwal dokter harus diisi',
            "g-recaptcha-response.required" => "Silahkan centang captcha terlebih dahulu",
            "g-recaptcha-response.captcha" => "Captcha tidak valid, silahkan tunggu beberapa saat kemudian centang captcha lagi",

        ];
    }
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(ApiHelper::jsonResponse(422, "Validasi error", $validator->errors()));
    }
}
