<?php

namespace App\Http\Requests;

use App\Helpers\Facades\ApiHelper;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class RegIdentitasReq extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            "no_ktp" => "required|digits:16",
            "tgl_lahir" => "required|date_format:d-m-Y",
            'g-recaptcha-response' => 'required|captcha',
        ];

    }
    public function messages()
    {
        return [
            "no_ktp.required" => 'No Ktp harus diisi',
            "no_ktp.digits" => 'No Ktp harus 16 digit',
            "tgl_lahir.required" => 'Tanggal lahir harus diisi',
            "tgl_lahir.date_format" => 'Format tanggal lahir (dd-mm-yyyy)',
            "g-recaptcha-response.required" => "Silahkan centang captcha terlebih dahulu",
            "g-recaptcha-response.captcha" => "Captcha tidak valid, silahkan tunggu beberapa saat kemudian centang captcha lagi",

        ];
    }
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(ApiHelper::jsonResponse(422, "Validasi error", $validator->errors()));
    }
}
