<?php

namespace App\Http\Controllers\Web;

use App\Helpers\Facades\CryptHelper;
use App\Http\Controllers\Controller;
use App\Services\Beckend\RegistrasiService;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class RiwBookingController extends Controller
{
    public function index(Request $request, RegistrasiService $registrasiService)
    {
        if (!$request->id) {
            return redirect("/identitas-riwayat-booking");
        }
        $id = urldecode(CryptHelper::decript($request->id));
        $arrId = explode("|", $id);
        $noKtp = @$arrId[0] ?? "xyz";
        $tanggalLahir = @$arrId[1] ?? "xyz";
        $tanggalAmbil = @$arrId[2] ?? "xyz";
        if ($tanggalAmbil == "xyz") {
            return redirect("/identitas-riwayat-booking");
        }
        $carbonTanggalAmbil = Carbon::createFromTimestamp($tanggalAmbil);
        $selisihWaktu = Carbon::now()->diff($carbonTanggalAmbil)->s;

        // link aktif hanya 15 detik
        if ($selisihWaktu >= 15) {
            return redirect("/identitas-riwayat-booking");
        }

        $http = $registrasiService->listByNikTanggalLahir($noKtp, $tanggalLahir);
        if (!@$http->data->booking) {
            return redirect("/identitas-riwayat-booking");
        }

        $data = [
            'title' => 'Riwayat booking',
            'booking' => $http->data->booking,
        ];
        return view("riw-booking", $data);
    }

}
