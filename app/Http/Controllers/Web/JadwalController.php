<?php

namespace App\Http\Controllers\Web;

use App\Exceptions\WebException;
use App\Http\Controllers\Controller;
use App\Services\Beckend\RefService;

class JadwalController extends Controller
{
    public function index(RefService $refService)
    {
        $apiData = $refService->getPoli();
        if ($apiData->metadata->status_code != 200) {
            throw new WebException("Gagal, mengambil data referensi", 500);
        }
        $data = [
            'title' => 'RSU ISLAM BOYOLALI',
            'masterPoli' => $apiData->data,
        ];
        return view("jadwal", $data);
    }

}
