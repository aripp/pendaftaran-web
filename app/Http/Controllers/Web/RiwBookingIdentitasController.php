<?php

namespace App\Http\Controllers\Web;

use App\Helpers\Facades\CryptHelper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class RiwBookingIdentitasController extends Controller
{
    public function index(Request $request)
    {
        if ($request->id) {
            $id = urldecode(CryptHelper::decript($request->id));
            $arrId = explode("|", $id);
            $tanggalAmbil = @$arrId[2] ?? "xyz";
            if ($tanggalAmbil != "xzy") {
                $carbonTanggalAmbil = Carbon::createFromTimestamp($tanggalAmbil);
                $selisihWaktu = Carbon::now()->diff($carbonTanggalAmbil)->s;

                if ($selisihWaktu <= 2) {
                    $params = urlencode($request->id);
                    return redirect("/riwayat-booking?id=$params");
                }
            }
        }
        $data = [
            'title' => 'Riwayat booking',
        ];
        return view("riw-booking-identitas", $data);
    }

}
