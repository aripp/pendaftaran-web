<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;

class AntreanController extends Controller
{
    public function index()
    {
        $data = [
            'title' => 'RSU ISLAM BOYOLALI',
        ];
        return view("antrean", $data);
    }

}
