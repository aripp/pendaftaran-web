<?php

namespace App\Http\Controllers\Web;

use App\Exceptions\WebException;
use App\Helpers\Facades\CryptHelper;
use App\Http\Controllers\Controller;
use App\Services\Beckend\PasienService;
use App\Services\Beckend\RefService;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class RegLamaController extends Controller
{
    public function index(Request $request, PasienService $pasienService, RefService $refService)
    {
        if (!$request->id) {
            return redirect("/identitas-pasien-lama");
        }
        $id = urldecode(CryptHelper::decript($request->id));
        $arrId = explode("|", $id);
        $noKtp = @$arrId[0] ?? "xyz";
        $tanggalLahir = @$arrId[1] ?? "xyz";
        $tanggalAmbil = @$arrId[2] ?? "xyz";
        if ($tanggalAmbil == "xyz") {
            return redirect("/identitas-pasien-lama");
        }
        $carbonTanggalAmbil = Carbon::createFromTimestamp($tanggalAmbil);
        $selisihWaktu = Carbon::now()->diff($carbonTanggalAmbil)->s;

        // link aktif hanya 15 detik
        if ($selisihWaktu >= 15) {
            return redirect("/identitas-pasien-lama");
        }

        $http = $pasienService->showByNoKtpAndTglLahir($noKtp, $tanggalLahir);
        if (!@$http->data->pasien) {
            return redirect("/identitas-pasien-lama");
        }

        $apiData = $refService->getDemografi();
        if ($apiData->metadata->status_code != 200) {
            throw new WebException("Gagal, mengambil data referensi", 500);
        }

        $data = [
            'title' => 'Pendaftaran pasien lama',
            'pasien' => $http->data->pasien,
            'masterAsuransi' => $apiData->data->refAsuransi,
        ];
        // dd($data);
        return view("reg-lama", $data);
    }

}
