<?php

namespace App\Http\Controllers\Web;

use App\Exceptions\WebException;
use App\Http\Controllers\Controller;
use App\Services\Beckend\RefService;

class RegBaruController extends Controller
{
    public function index(RefService $refService)
    {

        $apiData = $refService->getDemografi();
        if ($apiData->metadata->status_code != 200) {
            throw new WebException("Gagal, mengambil data referensi", 500);
        }
        // dd($apiData);
        $data = [
            'title' => 'Pendaftaran pasien baru',
            'masterKabupaten' => $apiData->data->refAlamatKabupaten,
            'masterAgama' => $apiData->data->refAgama,
            'masterEtnis' => $apiData->data->refEtnis,
            'masterBahasa' => $apiData->data->refBahasa,
            'masterPendidikan' => $apiData->data->refPendidikan,
            'masterPekerjaan' => $apiData->data->refPekerjaan,
            'masterPerkawinan' => $apiData->data->refPerkawinan,
            'masterAsuransi' => $apiData->data->refAsuransi,
        ];

        return view("reg-baru", $data);
    }

}
