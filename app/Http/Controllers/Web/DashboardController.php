<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index()
    {
        $data = [
            'title' => 'RSU ISLAM BOYOLALI',
        ];
        return view("dashboard", $data);
    }

}
