<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RegSuksesController extends Controller
{
    public function index(Request $request)
    {
        if (!$request->id) {
            return redirect("/");
        }
        $data = [
            'title' => 'Pendaftaran sukses',
        ];
        return view("reg-sukses", $data);
    }

}
