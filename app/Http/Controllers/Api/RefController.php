<?php

namespace App\Http\Controllers\Api;

use App\Helpers\Facades\ApiHelper;
use App\Http\Controllers\Controller;
use App\Services\Beckend\RefService;
use Illuminate\Http\Request;

class RefController extends Controller
{
    public function kecamatan(Request $request, RefService $refService)
    {
        if (!$request->kabupaten) {
            return ApiHelper::jsonResponse(422, "Parameter kabupaten harus diisi");
        }
        $http = $refService->getKecamatan($request->kabupaten);
        $returnData = [
            'master_kecamatan' => $http->data,
        ];
        return ApiHelper::jsonResponse(200, "Ok", $returnData);
    }

}
