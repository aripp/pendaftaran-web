<?php

namespace App\Http\Controllers\Api;

use App\Helpers\Facades\ApiHelper;
use App\Http\Controllers\Controller;
use App\Services\Beckend\KamarService;

class KamarController extends Controller
{
    public function list(KamarService $kamarService) {
        $http = $kamarService->list();
        if ($http->data) {
            $kamar = [];
            foreach ($http->data as $row) {
                if (array_key_exists("$row->bangsal_kd@$row->kelas", $kamar)) {
                    if ($row->status == "KOSONG") {
                        $kamar["$row->bangsal_kd@$row->kelas"]['kosong'] += 1;
                    }
                    $kamar["$row->bangsal_kd@$row->kelas"]['kapasitas'] += 1;
                } else {
                    $kamar["$row->bangsal_kd@$row->kelas"] = [
                        'bangsal_kd' => $row->bangsal_kd,
                        'bangsal_nama' => $row->bangsal_nama,
                        'kelas' => $row->kelas,
                        'kapasitas' => 1,
                        'kosong' => $row->status == "KOSONG" ? 1 : 0,
                    ];
                }
            }
            $kamar = collect($kamar)->sortKeys()->toArray();
            $kamar = array_values($kamar);

        } else {
            $kamar = [];
        }
        $returnData = [
            'kamar' => $kamar,
        ];
        return ApiHelper::jsonResponse(200, "Ok", $returnData);
    }

}
