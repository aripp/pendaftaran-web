<?php

namespace App\Http\Controllers\Api;

use App\Helpers\Facades\ApiHelper;
use App\Helpers\Facades\CryptHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegIdentitasReq;
use App\Services\Beckend\PasienService;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class PasienController extends Controller
{

    public function show(RegIdentitasReq $request, PasienService $pasienService)
    {
        $tanggalLahir = Carbon::create($request->tgl_lahir)->format("Y-m-d");
        $http = $pasienService->showByNoKtpAndTglLahir($request->no_ktp, $tanggalLahir);

        if (@$http->data->pasien) {
            $returnData = [
                'id' => urlencode(CryptHelper::encript($http->data->pasien->no_ktp . "|" . $http->data->pasien->tgl_lahir . "|" . Carbon::now()->timestamp)),
            ];
        } else {
            $returnData = null;
        }
        return ApiHelper::jsonResponse($http->metadata->status_code, $http->metadata->message, $returnData);

    }

}
