<?php

namespace App\Http\Controllers\Api;

use App\Helpers\Facades\ApiHelper;
use App\Http\Controllers\Controller;
use App\Services\Beckend\AntreanService;
use Illuminate\Http\Request;

class AntreanController extends Controller
{
    public function getByJadwalAndTanggal(Request $request, AntreanService $antreanService)
    {
        if (!$request->jadwal) {
            return ApiHelper::jsonResponse(500, "Parameter jadwal harus diisi");
        }
        if (!$request->tanggal) {
            return ApiHelper::jsonResponse(500, "Parameter tanggal harus diisi");
        }
        $http = $antreanService->getByJadwalAndTanggal($request->jadwal, $request->tanggal);
        $returnData = [
            'antrean' => $http->data,
        ];
        return ApiHelper::jsonResponse(200, "Ok", $returnData);
    }

}
