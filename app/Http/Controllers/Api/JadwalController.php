<?php

namespace App\Http\Controllers\Api;

use App\Helpers\Facades\ApiHelper;
use App\Http\Controllers\Controller;
use App\Services\Beckend\JadwalService;
use Illuminate\Http\Request;

class JadwalController extends Controller
{
    public function showByPoli(Request $request, JadwalService $jadwalService)
    {
        $http = $jadwalService->getByPoli($request->poli);
        $rtData = [];
        if($http->data){
            $masterHari = ['SENIN', 'SELASA', 'RABU', 'KAMIS', 'JUMAT', 'SABTU', 'MINGGU'];
            $rtData = collect($http->data)->groupBy(["dokter_nama", "poli_nama", "hari"])->toArray();
            $zDokter = [];
            foreach($rtData as $dokter => $arrPoli){
                foreach($arrPoli as $poli => $arrHari){
                    $dokter_kd = "";
                    $dokter_nama = "";
                    $dokter_image = "";
                    $poli_kd = "";
                    $poli_nama = "";
                    $zHari = [];
                    foreach($masterHari as $rowMasterHari){
                        $zJadwal = [];
                        if(array_key_exists($rowMasterHari, $arrHari)){
                            foreach($arrHari[$rowMasterHari] as $rowJadwalDetail){
                                $poli_kd = $rowJadwalDetail->poli_kd;
                                $poli_nama = $rowJadwalDetail->poli_nama;
                                $dokter_kd = $rowJadwalDetail->dokter_kd;
                                $dokter_image = file_exists(public_path('image/dokter/'.$rowJadwalDetail->dokter_kd .'.png')) ? '/image/dokter/'.$rowJadwalDetail->dokter_kd .'.png' : '/image/dokter/dokter.png' ;
                                $dokter_nama = $rowJadwalDetail->dokter_nama;
                                $zJadwal[] = [
                                    'jam_mulai' => $rowJadwalDetail->jam_mulai,
                                    'jam_selesai' => $rowJadwalDetail->jam_selesai,
                                    'waktu' => $rowJadwalDetail->waktu,
                                    'aktif' => $rowJadwalDetail->aktif,
                                    'sisa_app' => $rowJadwalDetail->sisa_app,
                                ];
                            }
                        }

                        $zHari[] = [
                            'hari' => $rowMasterHari,
                            'jadwal' => array_values(collect($zJadwal)->sortBy('jam_mulai', SORT_NUMERIC)->toArray()),
                        ];
                    }
                    if($poli_kd || $dokter_kd){
                        $zDokter[] = [
                            'dokter_kd' => $dokter_kd,
                            'dokter_nama' => $dokter_nama,
                            'dokter_image' => $dokter_image,
                            'poli_kd' => $poli_kd,
                            'poli_nama' => $poli_nama,
                            'hari' => $zHari
                        ];
                    }
                }

            }
            $rtData = $zDokter;
        }
        $returnData = [
            'jadwal' => $rtData,
        ];
        return ApiHelper::jsonResponse(200, "Ok", $returnData);
    }
    public function showByTanggal(Request $request, JadwalService $jadwalService)
    {
        if (!$request->tanggal) {
            return ApiHelper::jsonResponse(500, "Parameter tanggal harus diisi");
        }
        $http = $jadwalService->getByTanggal($request->tanggal);
        $returnData = [
            'jadwal' => $http->data,
        ];
        return ApiHelper::jsonResponse(200, "Ok", $returnData);
    }
    public function getByIdAndTanggal(Request $request, JadwalService $jadwalService)
    {
        if (!$request->id) {
            return ApiHelper::jsonResponse(500, "Parameter id harus diisi");
        }
        if (!$request->tanggal) {
            return ApiHelper::jsonResponse(500, "Parameter tanggal harus diisi");
        }
        $http = $jadwalService->getByIdAndTanggal($request->id, $request->tanggal);
        $returnData = [
            'jadwal' => $http->data,
        ];
        return ApiHelper::jsonResponse(200, "Ok", $returnData);
    }

}
