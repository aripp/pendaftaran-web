<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Http\Requests\RegSaveReq;
use App\Helpers\Facades\ApiHelper;
use App\Helpers\Facades\DateHelper;
use App\Helpers\Facades\CryptHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegIdentitasReq;
use App\Helpers\Facades\AsuransiHelper;
use App\Services\Beckend\RegistrasiService;

class RegController extends Controller
{

    public function save(RegSaveReq $request, RegistrasiService $registrasiService)
    {
        $nama = $request->nama;
        if (!$request->no_rm) {
            $namaType = DateHelper::namaTypeByUmur($request->tgl_lahir, $request->jenis_kelamin, $request->status_perkawinan);
            $nama = strtoupper($request->nama) . ", " . $namaType;
            return ApiHelper::jsonResponse(500, "Terimakasi telah mencoba menggunakan fitur ini, mohon maaf belum bisa digunakan 😬 . ");
        }
        if($request->asuransi_id == 3 ){
            return ApiHelper::jsonResponse(500, "Mohon maaf untuk asuransi BPJS silahkan pakai aplikasi Mobile JKN BPJS 😬 . ");
        }

        $umur = DateHelper::umur($request->tgl_lahir);
        $rawKunjungan = [
            'no_rm' => $request->no_rm,
            'nama' => $nama,
            'jenis_kelamin' => $request->jenis_kelamin,
            'tmp_lahir' => strtoupper($request->tmp_lahir),
            'tgl_lahir' => $request->tgl_lahir,
            'no_ktp' => $request->no_ktp,
            'bpjs_no_rujukan' => $request->no_rujukan,
            'agama' => $request->agama,
            'alamat' => strtoupper($request->alamat),
            'kota_kd' => $request->kota_kd,
            'kecamatan_kd' => $request->kecamatan_kd,
            'status_perkawinan' => $request->status_perkawinan,
            'pekerjaan' => $request->pekerjaan,
            'pendidikan' => $request->pendidikan,
            'bahasa' => $request->bahasa,
            'etnis' => $request->etnis,
            'nama_suami_istri' => strtoupper($request->nama_suami_istri),
            'nama_ibu' => strtoupper($request->nama_ibu),
            'nama_ayah' => strtoupper($request->nama_ayah),
            'no_telp' => $request->no_telp,
            'no_wa' => $request->no_wa,
            'email' => $request->email,

            'tanggal_masuk' => $request->tgl_masuk_only_date,
            'jadwal_dokter' => $request->jadwal_id,
            'cara_daftar' => "Aplikasi",
            'rujukan' => "DATANG SENDIRI",
            'asuransi_id' => $request->asuransi_id,
            'pj_nama' => $nama,
            'pj_no_ktp' => $request->no_ktp,
            'pj_umur' => $umur->tahun,
            'pj_alamat' => strtoupper($request->alamat),
            'pj_notelp' => $request->no_telp,
            'pj_hubungan' => "DIRI SENDIRI",
        ];

        $http = $registrasiService->save($rawKunjungan);

        if (@$http->data->id) {
            $returnData = [
                'id' => urlencode(CryptHelper::encript($http->data->id)),
            ];
        } else {
            $returnData = null;
        }
        return ApiHelper::jsonResponse($http->metadata->status_code, $http->metadata->message, $returnData);

    }
    public function listByNikTanggalLahir(RegIdentitasReq $request, RegistrasiService $registrasiService)
    {
        $tanggalLahir = Carbon::create($request->tgl_lahir)->format("Y-m-d");
        $http = $registrasiService->listByNikTanggalLahir($request->no_ktp, $tanggalLahir);
        if (@$http->data->booking) {
            $returnData = [
                'id' => urlencode(CryptHelper::encript($request->no_ktp . "|" . $tanggalLahir . "|" . Carbon::now()->timestamp)),
            ];
        } else {
            $returnData = null;
        }
        return ApiHelper::jsonResponse($http->metadata->status_code, $http->metadata->message, $returnData);
    }

}
