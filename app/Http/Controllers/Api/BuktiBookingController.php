<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Services\Pdf\BuktiBooking;
use App\Helpers\Facades\CryptHelper;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Services\Beckend\RegistrasiService;

class BuktiBookingController extends Controller
{
    public function print(Request $request, BuktiBooking $buktiBooking, RegistrasiService $registrasiService) {
        if (!$request->id) {
            response("Data tidak ditemukan", 404);
        }
        $id = urldecode(CryptHelper::decript($request->id));

        $http = $registrasiService->show($id);
        if ($http->metadata->status_code != 200) {
            response($http->metadata->message, 500);
        }
        $data = [
            'inden' => $http->data->inden,
        ];

        // header('Content-Type: application/pdf');
        // return $buktiBooking->print($data);

        $filePath = Storage::path("booking");
        if (!File::isDirectory($filePath)) {
            File::makeDirectory($filePath, 0777, true, true);
        }

        $fileName = $filePath ."/".$http->data->inden->id . ".pdf";
        $buktiBooking->print($data, $fileName);

        return response()->download($fileName,"rsi_booking_". $http->data->inden->id . ".pdf", ['Content-Type' => 'application/pdf'])->deleteFileAfterSend();
    }

}
