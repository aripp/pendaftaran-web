<?php

namespace App\Providers;

use App\Services\Pdf\BuktiBooking;
use Illuminate\Support\ServiceProvider;

class PdfServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->app->singleton(BuktiBooking::class, function ($app) {
            return new BuktiBooking();
        });

    }

    public function boot()
    {

    }
}
