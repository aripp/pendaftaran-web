<?php

namespace App\Providers;

use App\Helpers\ApiHelper;
use App\Helpers\AsuransiHelper;
use App\Helpers\CryptHelper;
use App\Helpers\DateHelper;
use Illuminate\Support\ServiceProvider;

class HelperServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->app->bind('ApiHelper', function ($app) {
            return new ApiHelper();
        });
        $this->app->bind('AsuransiHelper', function ($app) {
            return new AsuransiHelper();
        });
        $this->app->bind('DateHelper', function ($app) {
            return new DateHelper();
        });
        $this->app->bind('CryptHelper', function ($app) {
            return new CryptHelper();
        });

    }

    public function boot()
    {

    }
}
