<?php

namespace App\Providers;

use App\Services\Beckend\AntreanService;
use App\Services\Beckend\JadwalService;
use App\Services\Beckend\KamarService;
use App\Services\Beckend\PasienService;
use App\Services\Beckend\RefService;
use App\Services\Beckend\RegistrasiService;
use Illuminate\Support\ServiceProvider;

class BeckendServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->app->singleton(RefService::class, function ($app) {
            return new RefService();
        });
        $this->app->singleton(JadwalService::class, function ($app) {
            return new JadwalService();
        });
        $this->app->singleton(RegistrasiService::class, function ($app) {
            return new RegistrasiService();
        });
        $this->app->singleton(PasienService::class, function ($app) {
            return new PasienService();
        });
        $this->app->singleton(AntreanService::class, function ($app) {
            return new AntreanService();
        });
        $this->app->singleton(KamarService::class, function ($app) {
            return new KamarService();
        });

    }

    public function boot()
    {

    }
}
