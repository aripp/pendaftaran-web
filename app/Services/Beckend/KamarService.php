<?php
namespace App\Services\Beckend;

class KamarService extends HttpService
{

    public function __construct()
    {
        parent::__construct();
    }

    public function list() {
        $http = $this->get("api/kamar");
        return json_decode($http);
    }

}
