<?php
namespace App\Services\Beckend;

class PasienService extends HttpService
{

    public function __construct()
    {
        parent::__construct();
    }

    public function showByNoKtpAndTglLahir($noKtp, $tanggalLahir)
    {
        $http = $this->get("api/pasien/show-by-nik/$noKtp/tanggal-lahir/$tanggalLahir");
        return json_decode($http);
    }

}
