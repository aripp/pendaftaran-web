<?php
namespace App\Services\Beckend;

class JadwalService extends HttpService
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getByTanggal($tanggal)
    {
        $http = $this->get("api/jadwal/show-by-tanggal/$tanggal");
        return json_decode($http);
    }
    public function getByIdAndTanggal($id, $tanggal)
    {
        $http = $this->get("api/jadwal/show-by-id/$id/tanggal/$tanggal");
        return json_decode($http);
    }
    public function getByPoli($poli)
    {
        $http = $this->get("api/jadwal/show-by-poli/$poli");
        return json_decode($http);
    }

}
