<?php
namespace App\Services\Beckend;

class AntreanService extends HttpService
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getByTanggal($tanggal)
    {
        $http = $this->get("api/jadwal/show-by-tanggal/$tanggal");
        return json_decode($http);
    }
    public function getByJadwalAndTanggal($jadwal, $tanggal)
    {
        $http = $this->get("api/antrean/show-by-jadwal/$jadwal/tanggal/$tanggal");
        return json_decode($http);
    }

}
