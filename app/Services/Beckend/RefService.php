<?php
namespace App\Services\Beckend;

class RefService extends HttpService
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getDemografi()
    {
        $http = $this->get("api/ref/demografi-all");
        return json_decode($http);
    }
    public function getKecamatan($kabupaten)
    {
        $http = $this->get("api/ref/kecamatan/kabupaten/$kabupaten");
        return json_decode($http);
    }
    public function getPoli()
    {
        $http = $this->get("api/ref/poli");
        return json_decode($http);
    }

}
