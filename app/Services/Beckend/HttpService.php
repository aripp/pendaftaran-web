<?php
namespace App\Services\Beckend;

use App\Exceptions\ApiException;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class HttpService
{
    private $base_url;
    private $headers;

    public function __construct()
    {
        $this->base_url = env('BECKEND_API_URL');
        $this->headers = [
            'x-app-key' => env('BECKEND_API_TOKEN'),
            'content-type' => 'application/json',
        ];
    }

    protected function get(string $path, array $headers = [])
    {
        if (!empty($headers)) {
            $this->headers = array_merge($this->headers, $headers);
        }
        try {
            $url = $this->base_url . '/' . $path;
            $http = Http::withHeaders($this->headers)->get($url);
            if ($http->json() == null) {
                throw new ApiException("Gagal, response null ada masalah di webservice :(", 500);
            }
            if ($http->successful()) {
                return $http->body();
            } else {
                throw new ApiException($http->json("metadata.message"), $http->json("metadata.status_code"));
            }
        } catch (\Exception $e) {
            if (Str::contains($e->getMessage(), 'timed out') || Str::contains($e->getMessage(), 'not resolve host')) {
                throw new ApiException("Gagal terhubung ke server", 500);
            }
            throw new ApiException($e->getMessage(), $e->getCode());
        }
    }

    protected function post(string $path, array $data, array $headers = [])
    {
        if (!empty($headers)) {
            $this->headers = array_merge($this->headers, $headers);
        }
        try {
            $url = $this->base_url . '/' . $path;
            $http = Http::withHeaders($this->headers)->post($url, $data);
            if ($http->json() == null) {
                throw new ApiException("Gagal, response null ada masalah di webservice :(", 500);
            }
            if ($http->successful()) {
                return $http->body();
            } else {
                throw new ApiException($http->json("metadata.message"), $http->json("metadata.status_code"));
            }
        } catch (\Exception $e) {
            if (Str::contains($e->getMessage(), 'timed out') || Str::contains($e->getMessage(), 'not resolve host')) {
                throw new ApiException("Gagal terhubung ke server", 500);
            }
            throw new ApiException($e->getMessage(), $e->getCode());
        }
    }

    protected function put(string $path, array $data, array $headers = [])
    {
        if (!empty($headers)) {
            $this->headers = array_merge($this->headers, $headers);
        }
        try {
            $url = $this->base_url . '/' . $path;
            $http = Http::withHeaders($this->headers)->put($url, $data);
            if ($http->json() == null) {
                throw new ApiException("Gagal, response null ada masalah di webservice :(", 500);
            }
            if ($http->successful()) {
                return $http->body();
            } else {
                throw new ApiException($http->json("metadata.message"), $http->json("metadata.status_code"));
            }
        } catch (\Exception $e) {
            if (Str::contains($e->getMessage(), 'timed out') || Str::contains($e->getMessage(), 'not resolve host')) {
                throw new ApiException("Gagal terhubung ke server", 500);
            }
            throw new ApiException($e->getMessage(), $e->getCode());
        }
    }

    protected function delete(string $path, array $data, array $headers = [])
    {

        if (!empty($headers)) {
            $this->headers = array_merge($this->headers, $headers);
        }
        try {
            $url = $this->base_url . '/' . $path;
            $http = Http::withHeaders($this->headers)->delete($url, $data);
            if ($http->json() == null) {
                throw new ApiException("Gagal, response null ada masalah di webservice :(", 500);
            }
            if ($http->successful()) {
                return $http->body();
            } else {
                throw new ApiException($http->json("metadata.message"), $http->json("metadata.status_code"));
            }
        } catch (\Exception $e) {
            if (Str::contains($e->getMessage(), 'timed out') || Str::contains($e->getMessage(), 'not resolve host')) {
                throw new ApiException("Gagal terhubung ke server", 500);
            }
            throw new ApiException($e->getMessage(), $e->getCode());
        }
    }
}
