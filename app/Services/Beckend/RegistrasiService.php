<?php
namespace App\Services\Beckend;

class RegistrasiService extends HttpService
{

    public function __construct()
    {
        parent::__construct();
    }

    public function save($data)
    {
        $http = $this->post("api/registrasi/save", $data);
        return json_decode($http);
    }

    public function show($id)
    {
        $http = $this->get("api/registrasi/show-by-id/$id");
        return json_decode($http);
    }

    public function listByNikTanggalLahir($nik, $tanggalLahir)
    {
        $http = $this->get("api/registrasi/list-by-nik/$nik/tanggal-lahir/$tanggalLahir");
        return json_decode($http);
    }

}
