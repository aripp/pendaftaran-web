<?php
namespace App\Services\Pdf;

use Illuminate\Support\Str;
use Illuminate\Support\Carbon;
use App\Helpers\Facades\DateHelper;
use Illuminate\Support\Facades\File;
use App\Services\Pdf\Layout\PdfBlank;
use Illuminate\Support\Facades\Storage;

class BuktiBooking
{
    /**
     * @param array $data = [
     *      'inden' => (object) inden
     *      'filePath' => string
     * ]
     * @param bool $isDirect
     * @param string $printerName
     *
     * @return [type]
     */
    public function print($data, $fileName) {
        extract($data);

        $pdf = new PdfBlank('P', 'mm', 'A4', true, 'UTF-8', false);

        $title = "Bukti Booking $inden->id RSU Islam Boyolali";

        $pdf->SetMargins(12, 7, -1, true);
        $pdf->SetTitle($title);
        $pdf->AddPage();

        $pdf->SetLineWidth(0.3);

        // =========== GAMBAR ===============
        $logo = Storage::path('image/rsuib-logo.png');
        // =========== END GAMBAR ===========

        $pdf->Image($logo, 14, 7, 12, 12, 'PNG', '', '', true, 300, '', false, false, 0, false, false, false);

        $pdf->SetFont('helvetica', 'B', 12);
        $pdf->Cell(18, 0, '', 0, 0, 'L', 0, '', 0, false, 'T', 'M');
        $pdf->Cell(170, 0, 'RSU ISLAM BOYOLALI', 0, 1, 'L', 0, '', 0, false, 'T', 'M');
        $pdf->SetFont('helvetica', '', 8);
        $pdf->Cell(18, 0, '', 0, 0, 'L', 0, '', 0, false, 'T', 'M');
        $pdf->Cell(170, 0, 'Jl. Raya Boyolali-Klaten, Kemiri, Mojosongo, Boyolali', 0, 1, 'L', 0, '', 0, false, 'T', 'M');
        $pdf->Cell(18, 0, '', 0, 0, 'L', 0, '', 0, false, 'T', 'M');
        $pdf->Cell(176, 0, 'Email : rsuislamboyolali@gmail.com', 0, 1, 'L', 0, '', 0, false, 'T', 'M');

        $pdf->ln(3);
        $pdf->SetFont('helvetica', 'B', 12);
        $pdf->setX(10);
        $pdf->Cell(190, 10, "Bukti Booking Jadwal Dokter", 'T', 1, 'C', 0, '', 0);
        $pdf->ln(2);

        $pdf->SetFont('helvetica', 'I', 10);
        $pdf->Cell(0, 0, date('d/m/Y, H:i:s'), 0, 1, 'R', 0, '', 0);
        $pdf->SetFont('helvetica', 'B', 10);
        $pdf->Cell(0, 0, 'Booking info', 0, 1, 'L', 0, '', 0);

        $pdf->SetFont('helvetica', '', 10);
        $pdf->Cell(2, 0, '', 0, 0, 'L', 0, '', 0);
        $pdf->Cell(25, 0, 'Kode booking', 0, 0, 'L', 0, '', 0);
        $pdf->Cell(3, 0, ':', 0, 0, 'C', 0, '', 0);
        $pdf->SetFont('helvetica', 'B', 10);
        $pdf->SetTextColor(255, 0, 0);
        $pdf->Cell(85, 0, $inden->antrian_id, 0, 1, 'L', 0, '', 0);
        $pdf->SetTextColor(0, 0, 0);

        $pdf->SetFont('helvetica', '', 10);
        $pdf->Cell(2, 0, '', 0, 0, 'L', 0, '', 0);
        $pdf->Cell(25, 0, 'Poli', 0, 0, 'L', 0, '', 0);
        $pdf->Cell(3, 0, ':', 0, 0, 'C', 0, '', 0);
        $pdf->Cell(60, 0, $inden->poli_nama, 0, 1, 'L', 0, '', 0);

        $pdf->Cell(2, 0, '', 0, 0, 'L', 0, '', 0);
        $pdf->Cell(25, 0, 'Dokter', 0, 0, 'L', 0, '', 0);
        $pdf->Cell(3, 0, ':', 0, 0, 'C', 0, '', 0);
        $pdf->MultiCell(60, 0, $inden->dokter_nama, 0, 'L', 0, 1, '', '', true);

        $pdf->Cell(2, 0, '', 0, 0, 'L', 0, '', 0);
        $pdf->Cell(25, 0, 'Tanggal', 0, 0, 'L', 0, '', 0);
        $pdf->Cell(3, 0, ':', 0, 0, 'C', 0, '', 0);
        $pdf->Cell(60, 0, Carbon::create($inden->tgl_masuk)->format("d-m-Y"), 0, 1, 'L', 0, '', 0);

        $pdf->Cell(2, 0, '', 0, 0, 'L', 0, '', 0);
        $pdf->Cell(25, 0, 'Jam praktik', 0, 0, 'L', 0, '', 0);
        $pdf->Cell(3, 0, ':', 0, 0, 'C', 0, '', 0);
        $pdf->Cell(60, 0, "$inden->jam_mulai sd $inden->jam_selesai", 0, 1, 'L', 0, '', 0);

        $pdf->Cell(2, 0, '', 0, 0, 'L', 0, '', 0);
        $pdf->Cell(25, 0, 'Antrian', 0, 0, 'L', 0, '', 0);
        $pdf->Cell(3, 0, ':', 0, 0, 'C', 0, '', 0);
        $pdf->SetFont('helvetica', 'B', 10);
        $pdf->Cell(60, 0, $inden->antrean, 0, 1, 'L', 0, '', 0);

        $pdf->setY(40);
        $pdf->setX(110);

        $pdf->SetFont('helvetica', 'B', 10);
        $pdf->Cell(0, 0, 'Identitas pasien', 0, 1, 'L', 0, '', 0);

        $pdf->SetFont('helvetica', '', 10);

        $pdf->setX(110);
        $pdf->Cell(2, 0, '', 0, 0, 'L', 0, '', 0);
        $pdf->Cell(25, 0, 'No.RM', 0, 0, 'L', 0, '', 0);
        $pdf->Cell(3, 0, ':', 0, 0, 'C', 0, '', 0);
        $pdf->Cell(60, 0, $inden->no_rm ?? '-', 0, 0, 'L', 0, '', 0);
        $pdf->SetFont('helvetica', '', 10);
        $pdf->Cell(20, 0, '', 0, 1, 'L', 0, '', 0);

        $pdf->setX(110);
        $pdf->Cell(2, 0, '', 0, 0, 'L', 0, '', 0);
        $pdf->Cell(25, 0, 'NIK', 0, 0, 'L', 0, '', 0);
        $pdf->Cell(3, 0, ':', 0, 0, 'C', 0, '', 0);
        $pdf->Cell(60, 0, $inden->no_ktp ?? '-', 0, 1, 'L', 0, '', 0);

        $pdf->setX(110);
        $pdf->Cell(2, 0, '', 0, 0, 'L', 0, '', 0);
        $pdf->Cell(25, 0, 'Nama', 0, 0, 'L', 0, '', 0);
        $pdf->Cell(3, 0, ':', 0, 0, 'C', 0, '', 0);
        $pdf->MultiCell(65, 0, $inden->nama, 0, 'L', 0, 1, '', '', true);

        $umur = DateHelper::umur($inden->tgl_lahir);

        $pdf->setX(110);
        $pdf->Cell(2, 0, '', 0, 0, 'L', 0, '', 0);
        $pdf->Cell(25, 0, 'Tgl lahir', 0, 0, 'L', 0, '', 0);
        $pdf->Cell(3, 0, ':', 0, 0, 'C', 0, '', 0);
        $pdf->Cell(60, 0,  Carbon::create($inden->tgl_lahir)->format("d-m-Y") . " ($umur->tahun TH / $umur->bulan BLN)", 0, 1, 'L', 0, '', 0);

        $pdf->setX(110);
        $pdf->Cell(2, 0, '', 0, 0, 'L', 0, '', 0);
        $pdf->Cell(25, 0, 'Asuransi', 0, 0, 'L', 0, '', 0);
        $pdf->Cell(3, 0, ':', 0, 0, 'C', 0, '', 0);
        $pdf->Cell(60, 0, $inden->asuransi_nama, 0, 1, 'L', 0, '', 0);

        $pdf->ln(50);

        $pdf->SetFont('helvetica', 'B', 10);
        $pdf->Cell(0, 0, 'Ketentuan :', 0, 1, 'L', 0, '', 0);
        $pdf->SetFont('helvetica', '', 10);
        $pdf->Cell(2, 0, '', 0, 0, 'L', 0, '', 0);
        $pdf->Cell(4, 0, '1. ', 0, 0, 'L', 0, '', 0);
        $pdf->MultiCell(180, 0, "Datang 30 menit sebelum praktik dokter dimulai.", 0, 'L', 0, 1, '', '', true);

        if(Str::contains($inden->asuransi_nama, "BPJS")){
            $pdf->Cell(2, 0, '', 0, 0, 'L', 0, '', 0);
            $pdf->Cell(4, 0, '2. ', 0, 0, 'L', 0, '', 0);
            $pdf->MultiCell(180, 0, "Silahkan menuju mesin APM, pencet tulisan Masukan No. Induk Kependudukan (Aplikasi sidik jari BPJS) pada layar kemudian arahkan QRCODE NIK diatas ke mesin scan.", 0, 'L', 0, 1, '', '', true);
            // $pdf->Cell(2, 0, '', 0, 0, 'L', 0, '', 0);
            // $pdf->Cell(4, 0, '3. ', 0, 0, 'L', 0, '', 0);
            // $pdf->MultiCell(180, 0, "Scan <b>QRCODE NIK</b> dari bukti booking.", 0, 'L', 0, 1, '', '', true, 0, true);
            $pdf->Cell(2, 0, '', 0, 0, 'L', 0, '', 0);
            $pdf->Cell(4, 0, '3. ', 0, 0, 'L', 0, '', 0);
            $pdf->MultiCell(180, 0, "Jika sudah muncul data pasien dan sesuai silahkan scan sidik jari pasien pada mesin sidik jari (jempol kanan /Kiri).", 0, 'L', 0, 1, '', '', true);
            $pdf->Cell(2, 0, '', 0, 0, 'L', 0, '', 0);
            $pdf->Cell(4, 0, '4. ', 0, 0, 'L', 0, '', 0);
            $pdf->MultiCell(180, 0, "Jika keluar pesan 'sidik jari belum terdaftar' silahkan menuju ke loket pendaftaran dengan mengambil antrean BPJS terlebih dahulu.", 0, 'L', 0, 1, '', '', true);
            $pdf->Cell(2, 0, '', 0, 0, 'L', 0, '', 0);
            $pdf->Cell(4, 0, '5. ', 0, 0, 'L', 0, '', 0);
            $pdf->MultiCell(180, 0, "Jika keluar pesan 'berhasil scan sidik jari' lanjut pencet tulisan kode booking (Aplikasi checkin) pada layar kemudian arahkan QRCODE BOOKING diatas ke mesin scan.", 0, 'L', 0, 1, '', '', true, 0, true);
            $pdf->Cell(2, 0, '', 0, 0, 'L', 0, '', 0);
            $pdf->Cell(4, 0, '6. ', 0, 0, 'L', 0, '', 0);
            $pdf->MultiCell(180, 0, "Setelah berhasil akan mencetak Formulir pelayanan.", 0, 'L', 0, 1, '', '', true);
            $pdf->Cell(2, 0, '', 0, 0, 'L', 0, '', 0);
            $pdf->Cell(4, 0, '7. ', 0, 0, 'L', 0, '', 0);
            $pdf->MultiCell(180, 0, "Selanjutnya silahkan menuju skrining poliklinik dengan membawa Formulir pelayanan.", 0, 'L', 0, 1, '', '', true);
        }else{
            $pdf->Cell(2, 0, '', 0, 0, 'L', 0, '', 0);
            $pdf->Cell(4, 0, '2. ', 0, 0, 'L', 0, '', 0);
            $pdf->MultiCell(180, 0, "Silahkan menuju mesin APM, pencet tulisan kode booking (Aplikasi checkin) pada layar kemudian arahkan QRCODE BOOKING diatas ke mesin scan.", 0, 'L', 0, 1, '', '', true, 0, true);
            $pdf->Cell(2, 0, '', 0, 0, 'L', 0, '', 0);
            $pdf->Cell(4, 0, '3. ', 0, 0, 'L', 0, '', 0);
            $pdf->MultiCell(180, 0, "Setelah berhasil akan mencetak Formulir pelayanan.", 0, 'L', 0, 1, '', '', true);
            $pdf->Cell(2, 0, '', 0, 0, 'L', 0, '', 0);
            $pdf->Cell(4, 0, '4. ', 0, 0, 'L', 0, '', 0);
            $pdf->MultiCell(180, 0, "Selanjutnya silahkan menuju skrining poliklinik dengan membawa Formulir pelayanan.", 0, 'L', 0, 1, '', '', true);
        }
        $pdf->ln(10);
        $pdf->SetFont('helvetica', 'I', 10);

        $pdf->MultiCell(180, 0, "...Terimakasih...", 0, 'C', 0, 1, '', '', true);

        $style = array(
            'border' => 2,
            'vpadding' => 'auto',
            'hpadding' => 'auto',
            'fgcolor' => array(0,0,0),
            'bgcolor' => false, //array(255,255,255)
            'module_width' => 1, // width of a single module in points
            'module_height' => 1 // height of a single module in points
        );

        $pdf->setY(80);
        $pdf->write2DBarcode("$inden->antrian_id", 'QRCODE,H', 20, $pdf->getY(), 30, 30, $style, 'N');
        $pdf->SetFont('helvetica', 'B', 10);
        $pdf->Cell(47, 0, 'QRCODE BOOKING', 0, 0, 'C', 0, '', 0);

        $pdf->setY(80);
        $pdf->write2DBarcode("$inden->no_ktp", 'QRCODE,H', 160, $pdf->getY(), 30, 30, $style, 'N');
        $pdf->SetFont('helvetica', 'B', 10);
        $pdf->setX(143);
        $pdf->Cell(64, 0, 'QRCODE NIK', 0, 0, 'C', 0, '', 0);


       return $pdf->Output($fileName, 'F');
    }

}
