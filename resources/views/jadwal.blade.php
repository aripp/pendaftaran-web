@extends('layouts.app')
@section('title', $title)

@section('content')
    <div class="container-lg">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow">
                    <div class="card-header">
                        <a href="{{ route('dashboard') }}" class="text-decoration-none text-reset"><i class="fas fa-arrow-left mr-1"></i></a>
                        Jadwal
                    </div>
                    <div class="card-body">
                        <div class="mb-3">
                            <p class="m-0 text-muted font-weight-bold"><b>Keterangan</b></p>
                            <ul class="px-3" style="font-size: 0.9rem;">
                                <li>Jadwal {{ Carbon\Carbon::now()->startOfDay()->isoFormat('dddd, D MMM Y') }} sd {{ Carbon\Carbon::now()->addDays(6)->startOfDay()->isoFormat('dddd, D MMM Y') }}</li>
                                <li>Untuk booking jadwal silahkan klik <a href="{{ route('reg.lama.identitas') }}" class="badge badge-success"> link ini</a></li>
                                <li>Jika ada pertanyaan silahkan hubungi 0812-2517-6300 atau klik <a href="https://api.whatsapp.com/send?phone=6281225176300" class="badge badge-success"> link ini</a> </li>
                            </ul>
                        </div>
                        <div class="form-group">
                            <label for="" class="font-weight-bold text-muted">Poliklinik</label>
                            <select class="form-control" id="poli">
                                <option value=""></option>
                                @foreach ($masterPoli as $poli)
                                <option value="{{ $poli->poli_kd }}">{{ $poli->poli_nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <p class="text-muted font-weight-bold">Jadwal</p>
                        <div id="wrap_jadwal" class="row">
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section("javascript")
    <script type="text/javascript">
    const jadwal = (function($) {
        $(document).ready( e => {
            getJadwal()

        })
        $("#poli").select2({
            placeholder: 'Poliklinik',
            allowClear: true,
            theme: 'bootstrap4'
        })
        $("#poli").change( e => {
            let poli = $("#poli").val()
            getJadwal(poli)
        })
        function getJadwal(poli = ''){
            $.ajax({
                method: 'GET',
                dataType: 'JSON',
                url: window.origin + '/api/jadwal/show-by-poli/' + poli,
                beforeSend(xhr) {
                    $("#wrap_jadwal").html(`<div class="col-md-12">
                                            <div class="card">
                                                <div class="card-body">Loading ...
                                                </div>
                                            </div>
                                        </div>`)
                },
                success: function (result) {
                    let html = ""
                    if(result.data.jadwal.length > 0){
                        result.data.jadwal.forEach( dokter => {
                            html += `<div class="col-md-6">
                                            <div class="card my-2 mx-2">
                                                <div class="card-body">
                                                    <div class="">
                                                        <h6 class="font-weight-bold text-center text-muted">${dokter.dokter_nama}</h6>
                                                        <p class="font-weight-bold text-center text-muted" style="font-size: 0.8rem">${dokter.poli_nama}</p>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-5 mb-3">
                                                            <img src="${dokter.dokter_image}?v=1.0.0" class="image-thumbnail shadow rounded d-block my-auto mx-auto" style="height: 14rem"/>
                                                        </div>
                                                        <div class="col-md-7">
                                                            <table class="table table-sm" style="font-size: 0.7rem">
                                                                <thead>
                                                                    <tr>
                                                                        <th colspan=2>Jadwal</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                    `
                            dokter.hari.forEach( hari => {
                                html += `
                                            <tr>
                                                <td>${hari.hari}</td>
                                                <td>
                                        `
                                if(hari.jadwal.length > 0){
                                    let jam = ""
                                    hari.jadwal.forEach( (jadwal, i) =>{
                                        jam = `
                                                ${jadwal.jam_mulai.substring(0, 5)} - ${jadwal.jam_selesai.substring(0, 5)} (${jadwal.waktu})
                                            `
                                        if(i >= 1){
                                            html += `</br>`
                                        }
                                        if(jadwal.aktif == "jadwal belum dibuat"){
                                            html +=`<s> ${jam} </s> <span class="badge badge-dark">belum dibuat</span> `
                                        }else if(jadwal.aktif == 0){
                                            html +=`<s> ${jam} </s> <span class="badge badge-dark">izin</span> `
                                        }else if(jadwal.kuota <= 0){
                                            html +=`<s> ${jam} </s> <span class="badge badge-dark">kuota habis</span> `
                                        }else{
                                            html += jam
                                        }
                                    })
                                }else{
                                    html += `-`
                                }

                                html += `</td></tr>`

                            })

                            html += `
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                                `
                        })
                    }else{
                        html = ` <div class="col-md-12">
                                            <div class="card">
                                                <div class="card-body">Data tidak ditemukan
                                                </div>
                                            </div>
                                        </div>`
                    }
                    $("#wrap_jadwal").html(html)
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $("#wrap_jadwal").html(`<div class="col-md-12">
                                            <div class="card">
                                                <div class="card-body">Data tidak ditemukan
                                                </div>
                                            </div>
                                        </div>`)
                    toastr.error(jqXHR.responseJSON?.metadata?.message || jqXHR.statusText)
                },
            });
        }

    })(jQuery)
    </script>
@endsection

