@extends('layouts.app')
@section('title', $title)

@section('content')
    <div class="container-lg">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow">
                    <div class="card-header">
                        <a href="{{ route('dashboard') }}" class="text-decoration-none text-reset"><i class="fas fa-arrow-left mr-1"></i></a>
                        Ketersediaan kamar
                    </div>
                    <div class="card-body">
                    <div class="mb-3">
                        <p class="m-0 text-muted font-weight-bold" ><b>Keterangan</b></p>
                            <ul class="px-3" style="font-size: 0.9rem;">
                                <li>KSG = Kosong</li>
                                <li>KPS = Kapasitas</li>
                                <li>Jika ada pertanyaan silahkan hubungi 0812-2517-6300 atau klik <a href="https://api.whatsapp.com/send?phone=6281225176300" class="badge badge-success"> link ini</a> </li>
                            </ul>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-sm" style="font-size: 0.8rem">
                                <thead>
                                    <tr>
                                        <th>Kamar</th>
                                        <th>KSG</th>
                                        <th>KPS</th>
                                    </tr>
                                </thead>
                                <tbody id="tbl_kamar">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section("javascript")
    <script type="text/javascript">
    const riwBooking = (function($) {
        $(document).ready( e => {
            getKamar()

        })
        function getKamar(){
            $.ajax({
                method: 'GET',
                dataType: 'JSON',
                url: window.origin + '/api/kamar',
                beforeSend(xhr) {
                    $("#tbl_kamar").html(`<tr><td  colspan=3>Loading ...</td></tr>`)
                },
                success: function (result) {
                    let html = ""
                    if(result.data.kamar.length > 0){
                        result.data.kamar.forEach(e =>{
                            html += `
                                    <tr>
                                        <td>${e.bangsal_nama} <b>${e.kelas}</b></td>
                                        <td>${e.kosong}</td>
                                        <td>${e.kapasitas}</td>
                                    </tr>
                            `
                        })
                    }else{
                        html = "<tr><td  colspan=3>data tidak ditemukan</td></tr>"
                    }
                    $("#tbl_kamar").html(html)
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $("#tbl_kamar").html(`<tr><td  colspan=3>data tidak ditemukan</td></tr>`)
                    toastr.error(jqXHR.responseJSON?.metadata?.message || jqXHR.statusText)
                },
            });
        }

    })(jQuery)
    </script>
@endsection

