@extends('layouts.error')
@section('title', "Maaf sedang dalam perbaikan")

@section('content')
    <div class="container">
        <div style="height:5rem"></div>
        <h4 class="text-center">Maaf situs sedang dalam perbaikan</h4>
        <div class="text-center">
            <img src="{{asset("image/maintenance.png")}}" alt="rsu islam boyolali" class="img-fluid rounded" style="height: 25rem">
        </div>
    </div>
@endsection
