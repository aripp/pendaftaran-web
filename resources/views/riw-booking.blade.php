@extends('layouts.app')
@section('title', $title)

@section('content')
    <div class="container-lg">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow">
                    <div class="card-header">
                        <a href="{{ route('dashboard') }}" class="text-decoration-none text-reset"><i class="fas fa-arrow-left mr-1"></i></a>
                        Riwayat booking
                    </div>
                    <div class="card-body">
                        <p class="mb-1">Keterangan : </p>
                        <ul class="list-unstyled" style="font-size: 0.8rem">
                            <li><i class="fas fa-file-pdf text-muted mr-2" style="font-size: 1.5rem"></i> Sudah checkin, bukti booking tidak bisa didownload</li>
                            <li><i class="fas fa-file-pdf text-success mr-2" style="font-size: 1.5rem"></i> Belum checkin, bukti booking bisa didownload</li>
                        </ul>
                        <div class="table-responsive">
                            <table class="table table-striped table-sm">
                                <thead>
                                    <tr>
                                        <th>File</th>
                                        <th>Jadwal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($booking as $row)

                                        <tr>
                                        @if ($row->checkin == 1)
                                            <td class="text-center" style="vertical-align: middle;">
                                                <span><i class="fas fa-file-pdf text-muted" style="font-size: 1.5rem;"></i></span>
                                            </td>
                                        @else
                                            <td onclick="riwBooking.download('{{ urlencode((new \App\Helpers\CryptHelper)->encript($row->id)) }}')" class="text-center" style="vertical-align: middle;">
                                                <span><i class="fas fa-file-pdf text-success" style="font-size: 1.5rem;"></i></span>
                                            </td>
                                        @endif
                                            <td>{{ $row->dokter_nama }} <br/> {{ $row->tgl_masuk }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section("javascript")
    <script type="text/javascript">
    const riwBooking = (function($) {

        function download(id){
            window.location.replace(window.origin + '/api/bukti-booking?id=' + id);
        }
        return{
            download
        }

    })(jQuery)
    </script>
@endsection

