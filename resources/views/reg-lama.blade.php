@extends('layouts.app')
@section('title', $title)
@section('head')
    {!! NoCaptcha::renderJs() !!}
@endsection

@section('content')
    <div class="container-lg">
        <form id="formulir" class="row" autocomplete="off">
                <div class="col-md-12 py-2">
                    <nav>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}" class="text-decoration-none text-reset"><i class="fas fa-arrow-left mr-1"></i>Menu</a></li>
                            <li class="breadcrumb-item active">Pendaftaran pasien lama</li>
                        </ol>
                    </nav>
                </div>
                <div class="col-md-7 py-2">
                    <div class="card shadow">
                        <div class="card-header">
                            Data pasien
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label class="form-label">No rekam medis</label>
                                <input type="text" class="form-control" id="no_rm" name="no_rm" value="{{ $pasien->no_rm }}" readonly>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Nama</label>
                                <input type="text" class="form-control" id="nama" name="nama" value="{{ $pasien->nama }}"  readonly>
                            </div>
                            <div class="form-group">
                                <label class="form-label">No KTP/NIK</label>
                                <input type="text" class="form-control" id="no_ktp" name="no_ktp" value="{{ $pasien->no_ktp }}" readonly>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Tempat, tanggal lahir</label>
                                <div class="input-group input-group">
                                    <input type="text" class="form-control w-50" id="tmp_lahir" name="tmp_lahir" value="{{ $pasien->tmp_lahir }}" readonly>
                                    <input type="text" class="form-control w-50" id="tgl_lahir" name="tgl_lahir" value="{{ Carbon\Carbon::parse($pasien->tgl_lahir)->format('d-m-Y') }}" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Alamat Lengkap</label>
                                <textarea class="form-control" id="alamat" name="alamat" readonly > {{ $pasien->alamat }}</textarea >
                            </div>
                            <input type="hidden" name="jenis_kelamin" value="{{ $pasien->jenis_kelamin }}" readonly>
                            <input type="hidden" name="agama" value="{{ $pasien->agama }}" readonly>
                            <input type="hidden" name="etnis" value="{{ $pasien->etnis }}" readonly>
                            <input type="hidden" name="bahasa" value="{{ $pasien->bahasa }}" readonly>
                            <input type="hidden" name="pendidikan" value="{{ $pasien->pendidikan }}" readonly>
                            <input type="hidden" name="pekerjaan" value="{{ $pasien->pekerjaan }}" readonly>
                            <input type="hidden" name="status_perkawinan" value="{{ $pasien->status_perkawinan }}" readonly>
                            <input type="hidden" name="nama_ibu" value="{{ $pasien->nama_ibu }}" readonly>
                            <input type="hidden" name="nama_ayah" value="{{ $pasien->nama_ayah }}" readonly>
                            <input type="hidden" name="nama_suami_istri" value="{{ $pasien->nama_suami_istri }}" readonly>
                            <input type="hidden" name="kota_kd" value="{{ $pasien->kota_kd }}" readonly>
                            <input type="hidden" name="kecamatan_kd" value="{{ $pasien->kecamatan_kd }}" readonly>
                            <input type="hidden" name="no_telp" value="{{ $pasien->no_telp }}" readonly>
                            <input type="hidden" name="no_wa" value="{{ $pasien->no_wa }}" readonly>
                            <input type="hidden" name="email" value="{{ $pasien->email }}" readonly>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 py-2">
                    <div class="card shadow">
                        <div class="card-header">
                            Jadwal dan Cara bayar
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label class="form-label">Cara bayar/Asuransi</label>
                                <select class="form-control my-select2-nc" id="asuransi_id" name="asuransi_id">
                                    <option value=""></option>
                                    @foreach ($masterAsuransi as $item)
                                    <option value="{{ $item->asuransi_id }}">{{ $item->asuransi_nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group" id="wrap_no_rujukan">
                                <label class="form-label">No Rujukan</label>
                                <input type="tel" class="form-control" id="no_rujukan" name="no_rujukan">
                                <small class="form-text text-muted">
                                    <button type="button" id="btn_rujukan_contoh" class="btn btn-link">Lihat contoh no rujukan</button>
                                </small>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Tanggal Periksa</label>
                                <input type="text" class="form-control datetimepicker-input" id="tgl_masuk" name="tgl_masuk" data-toggle="datetimepicker">
                            </div>
                            <div class="form-group">
                                <label class="form-label">Jadwal</label>
                                <div class="spinner-border text-success" id="jadwal_loading"><span class="sr-only">Loading...</span></div>
                                <select class="form-control" id="jadwal_id" name="jadwal_id">
                                    <option value=""></option>
                                </select>
                            </div>
                            <div id="jadwal_detail"></div>
                        </div>
                    </div>
                    <div class="card shadow mt-3" id="w_submit">
                        <div class="card-body">
                                {!! NoCaptcha::display() !!}
                        </div>
                        <div class="card-footer">
                            <div class="text-center">
                                <button class="btn btn-dark shadow-lg" id="btn_form_daftar">Daftar Sekarang</button>
                            </div>
                        </div>
                    </div>
                </div>
        </form>
    </div>

<!-- Modal -->
<div class="modal fade" id="mdl_ketentuan" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Kebijakan & Ketentuan</h5>
      </div>
      <div class="modal-body">
        <ol>
            <li>Setuju dan memberi izin kepada dokter yang bersangkutan untuk merawat, mengobati sesuai dengan prosedur diagnostik yang berlaku di RSU Islam Boyolali.</li>
            <li>Setuju dengan tindakan-tindakan yang lazim dilakukan di RSU Islam Boyolali, seperti pemasangan infus, Selang lambung/NGT, selang kateter, injeksi dan medikasi.</li>
            <li>Sanggup/bersedia membayar seluruh biaya perawatan, pengobatan dan biaya administrasi.</li>
            <li>Memberi kuasa kepada dokter/pihak RSU Islam Boyolali untuk memberikan keterangan secukupnya yang diperlukan oleh penanggung biaya perawatan saya/pasien tersebut diatas.</li>
            <li>Saya menyatakan bahwa saya telah menerima informasi dan mengetahui tentang adanya tata cara mengajukan keluhan dengan cara SMS ke No. +62 813-9269-8124, Email: humas_rsuiboyolali@gmail.com atau bisa menulis langsung dan diserahkan kepada petugas RS.</li>
        </ol>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-secondary" id="btn_ketentuan_tidak_setuju">Tidak setuju</button>
        <button type="button" class="btn btn-dark" id="btn_ketentuan_setuju">Setuju</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="mdl_mjkn" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Pengumuman</h5>
      </div>
      <div class="modal-body">
        <p>Mohon maaf untuk asuransi BPJS silahkan pakai aplikasi <b>Mobile JKN BPJS</b>. Jika belum punya aplikasi <b>Mobile JKN BPJS</b> silahkan klik link dibawah ini untuk mendownload. </p>
        <ul>
            <li><a href="https://play.google.com/store/apps/details?id=app.bpjs.mobile">link untuk smartphone android </a> (playstore)</li>
            <li><a href="https://apps.apple.com/id/app/mobile-jkn/id1237601115"> link untuk smartphone iphone </a> (appstore)</li>
        </ul>
        <p>Untuk cara menggunakan silahkan klik link video ini <a href=" https://youtu.be/uhPr2SnyBdw" target="_blank">https://youtu.be/uhPr2SnyBdw</a> </p>
        <p>Terimakasih ...</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="mdl_rujukan_contoh" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Contoh no rujukan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
      </div>
      <div class="modal-body">
        <p class="font-weight-bold">Rujukan faskes 1 (Puskesmas/Klinik)</p>
        <img src="/image/bpjs/rujukan-pcare.jpg" alt="" class="img-fluid">
        <p class="font-weight-bold">Rujukan rumahsakit </p>
        <img src="/image/bpjs/rujukan-rs.png" alt="" class="img-fluid">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>
@endsection

@section("javascript")
    <script type="text/javascript">
    const cekIdentitas = (function($) {
        const ASURANSI_BPJS = [2, 3, 4]
        const TANGGAL_NOW = moment().startOf("day");
        const TANGGAL_MIN_INDEN = moment().startOf("day").add(1, 'days');
        const TANGGAL_MAX_INDEN = moment().startOf("day").add(1, 'months');

        $(document).ready(function() {
            // $("#mdl_ketentuan").modal('show')
            $("#jadwal_loading").addClass("d-none")
            $("#jadwal_detail").html("")
            $('#wrap_no_rujukan').addClass('d-none')
            getJadwal(TANGGAL_MIN_INDEN)
            $('#btn_form_daftar').text("Daftar Sekarang")
            $('#btn_form_daftar').prop("disabled", false)
        })
        $('#tgl_masuk').datetimepicker({
            format: 'dddd, DD-MM-YYYY',
            maxDate: TANGGAL_MAX_INDEN,
            minDate: TANGGAL_MIN_INDEN
        })
        $('#asuransi_id').change(e => {
            let asuransi = parseInt($('#asuransi_id').val())
            $('#no_rujukan').val('')
            if(ASURANSI_BPJS.includes(asuransi)){
                // $('#wrap_no_rujukan').removeClass('d-none')
                $('#mdl_mjkn').modal("show")
                $('#w_submit').addClass('d-none')
            }else{
                $('#w_submit').removeClass('d-none')
                $('#wrap_no_rujukan').addClass('d-none')
            }
        })
        $('#no_rujukan').inputmask('*******************');
        $("#btn_rujukan_contoh").click( e => {
            e.preventDefault()
            $("#mdl_rujukan_contoh").modal('show')
        })
        $('#tgl_masuk').on('hide.datetimepicker', function (e) {
            let tanggal = $('#tgl_masuk').val()
            getJadwal(tanggal)
        })
        function getJadwal(tanggal){
            $("#jadwal_detail").html("")
            $("#jadwal_id option").remove()
            $("#jadwal_id").html("<option values=''></option>")
            if(!tanggal) return

            $.ajax({
                method: 'GET',
                dataType: 'JSON',
                url: window.origin + '/api/jadwal/show-by-tanggal/' +  moment(tanggal, "dddd, DD-MM-YYYY").format("YYYY-MM-DD"),
                beforeSend(xhr) {
                    $("#jadwal_loading").removeClass("d-none")
                },
                success: function (result) {
                    $("#jadwal_loading").addClass("d-none")
                    let data = []
                    if(result.data.jadwal.length > 0){
                        data = result.data.jadwal.map( e => ({
                            id: e.id,
                            text: `${e.dokter_nama} <span class="badge badge-success"> ${e.poli_nama} ${e.jam_mulai} - ${e.jam_selesai} (${e.waktu})</span> `
                        }))
                    }
                    $('#jadwal_id').select2({
                        placeholder: '',
                        allowClear: true,
                        theme: 'bootstrap4',
                        data:data,
                        escapeMarkup: (markup) => markup,
                        templateResult: (data) => data.text,
                        templateSelection: (data) => data.text
                    });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $("#jadwal_loading").addClass("d-none")
                    toastr.error(jqXHR.responseJSON?.metadata?.message || jqXHR.statusText)
                },
            });
        }
        $("#jadwal_id").change( e => {
            let jadwal_id = $("#jadwal_id").val()
            let tanggal = $('#tgl_masuk').val()

            getJadwalDetail(jadwal_id, tanggal)
        })
        function getJadwalDetail(id, tanggal){
            $("#jadwal_detail").html("")
            if(!id) return

            $.ajax({
                method: 'GET',
                dataType: 'JSON',
                url: window.origin + '/api/jadwal/show-by-id/' +  id +'/tanggal/' + moment(tanggal, "dddd, DD-MM-YYYY").format("YYYY-MM-DD"),
                beforeSend(xhr) {
                    $("#jadwal_detail").html(`<div class="spinner-border text-success"><span class="sr-only">Loading...</span></div>`)
                },
                success: function (result) {
                    let html = ""
                    if(result.data.jadwal){
                        html = `
                                        <table class="table table-sm">
                                            <thead>
                                                <tr>
                                                    <th colspan=2>Detail Jadwal</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="w-25">Dokter</td>
                                                    <td class="w-75">${result.data.jadwal.dokter_nama}</td>
                                                </tr>
                                                <tr>
                                                    <td class="w-25">Poli</td>
                                                    <td class="w-75">${result.data.jadwal.poli_nama}</td>
                                                </tr>
                                                <tr>
                                                    <td class="w-25">Waktu</td>
                                                    <td class="w-75">${moment(result.data.jadwal.tanggal, "YYYY-MM-DD").format("dddd, DD-MM-YYYY")} <br/>${result.data.jadwal.jam_mulai} sd ${result.data.jadwal.jam_selesai} </td>
                                                </tr>
                                                <tr>
                                                    <td class="w-25">Kuota</td>
                                                    <td class="w-75">${result.data.jadwal.sisa}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                        `
                    }else{
                        html = ""
                    }
                    $("#jadwal_detail").html(html)
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $("#jadwal_detail").html("")
                    toastr.error(jqXHR.responseJSON?.metadata?.message || jqXHR.statusText)
                },
            });
        }
        $('#btn_form_daftar').click( e => {
            e.preventDefault()

            let form =  $("#formulir").serialize();
            form += '&tgl_masuk_only_date=' + moment($("#tgl_masuk").val(),'dddd, DD-MM-YYYY').format("DD-MM-YYYY")

            Swal.fire({
                text: "Proses, mohon tunggu ...",
                showConfirmButton: false,
                allowEscapeKey: false,
                allowOutsideClick: false,
            })

            $.ajax({
                method: 'POST',
                dataType: 'JSON',
                data: form,
                url: window.origin + '/api/pendaftaran-save',
                beforeSend(xhr) {
                    $('#btn_form_daftar').text("Mendaftar ...")
                    $('#btn_form_daftar').prop("disabled", true)
                },
                success: function (result) {
                    Swal.close()
                    $('#btn_form_daftar').text("Daftar Sekarang")
                    $('#btn_form_daftar').prop("disabled", false)

                    let id = result.data.id

                    Swal.fire({
                        icon:'success',
                        html:`<h5 class="">${result.metadata.message}</h5>`,
                        width: 530,
                        showConfirmButton: false,
                        allowEscapeKey: false,
                        allowOutsideClick: false,
                        timer: 1000
                    }).then(() => {
                       window.location.replace(window.origin + '/pendaftaran-berhasil?id=' + id);
                    })
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    Swal.close()

                    $('#btn_form_daftar').text("Daftar Sekarang")
                    $('#btn_form_daftar').prop("disabled", false)
                    if(jqXHR.status == 422){
                        let error = jqXHR.responseJSON?.data || []
                        let errorMessage = ''
                        for (const property in error) {
                            errorMessage += `${error[property]}, `;
                        }
                        Swal.fire({
                            icon: 'error',
                            title: 'Form Validasi',
                            text: errorMessage,
                            showConfirmButton: false,
                            footer: `${jqXHR.status} - ${jqXHR.statusText}`
                        })
                        return false;
                    }
                    toastr.error(jqXHR.responseJSON?.metadata?.message || jqXHR.statusText)
                },
            });


        })
        $("#btn_ketentuan_tidak_setuju").click( e => {
            window.location.replace(window.origin);
        })
        $("#btn_ketentuan_setuju").click( e => {
            $("#mdl_ketentuan").modal('hide')
        })

    })(jQuery)
    </script>
@endsection

