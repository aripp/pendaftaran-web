<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />

	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta content="#AEF2D2" name="theme-color"/>
	<meta content="rsu islam boyolali, rsi boyolali, rs boyolali, rumah sakit boyolali, jadwal dokter boyolali" name="description"/>
	<meta content="https://twitter.com/eestehh" name="author" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="icon" href="/image/icon-rsi.png" type="image/icon type">
    <title>@yield('title', 'RSU ISLAM BOYOLALI')</title>

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <script src="{{ asset('js/app.js') }}"></script>
    @yield('head', '')
</head>
<body>
    <div class="text-center mb-5 pt-3">
        <img src="{{ asset('image/rsiboyolali.png') }}" alt="RSU ISLAM BOYOLALI" style="height: 5rem" class="mb-3">
        <h5 class="text-muted">Ramah Amanah Profesional Islami</h5>
    </div>
    @yield("content")

    <div class="text-center mt-4">
        <p class="font-weight-bold text-white-50" style="font-size: 0.7rem">2022 - {{ date("Y") }} &#169; RSU ISLAM BOYOLALI</p>
    </div>

    <script type="text/javascript">
        $(".my-select2-nc").select2({
            placeholder: '',
            theme: 'bootstrap4',
            allowClear: false
        })
    </script>
    @yield("javascript")
</body>
</html>
