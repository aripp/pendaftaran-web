@extends('layouts.app')
@section('title', $title)
@section('head')
    {!! NoCaptcha::renderJs() !!}
@endsection

@section('content')
    <div class="container-lg">
        <div class="row">
            <div class="col-md-6 mx-auto">
                <div class="card shadow">
                    <div class="card-header">
                        <a href="{{ route('dashboard') }}" class="text-decoration-none text-reset"><i class="fas fa-arrow-left mr-1"></i></a>
                        Identitas Pasien
                    </div>
                    <div class="card-body">
                        <form id="formulir">

                            <div class="form-group">
                                <label class="form-label">NIK / No KTP</label>
                                <input type="tel" class="form-control" id="no_ktp" name="no_ktp">
                            </div>
                            <div class="form-group">
                                <label class="form-label">Tanggal lahir</label>
                                <input type="tel" class="form-control" id="tgl_lahir" name="tgl_lahir" placeholder="DD-MM-YYYY">
                            </div>
                            <div class="form-group">
                                {!! NoCaptcha::display() !!}
                            </div>

                        </form>
                    </div>
                    <div class="card-footer">
                        <div class="text-center">
                            <button class="btn btn-dark shadow-lg" id="btn_form_cek">Cek Identitas</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section("javascript")
    <script type="text/javascript">
    const cekIdentitas = (function($) {
        $('#no_ktp').inputmask('9999999999999999');
        $('#tgl_lahir').inputmask('99-99-9999');

         $('#btn_form_cek').click( e => {

            let form =  $("#formulir").serialize();

            $.ajax({
                method: 'POST',
                dataType: 'JSON',
                data: form,
                url: window.origin + '/api/pendaftaran-cek-riwayat',
                beforeSend(xhr) {
                    $('#btn_form_cek').text("Cek Identitas ...")
                    $('#btn_form_cek').prop("disabled", true)
                },
                success: function (result) {
                    $('#btn_form_cek').text("Cek Identitas")
                    $('#btn_form_cek').prop("disabled", false)

                    let id = result.data?.id
                    if(id){
                        Swal.fire({
                            icon:'success',
                            title: 'Identitas Pasien',
                            html:`<h5 class="">Data booking ditemukan</h5>`,
                            width: 530,
                            showConfirmButton: false,
                            allowEscapeKey: false,
                            allowOutsideClick: false,
                            timer: 1000
                        }).then(() => {
                            window.location.replace(window.origin + '/riwayat-booking?id=' + id);
                        })
                    }else{
                        Swal.fire({
                            icon: 'error',
                            width: 530,
                            title: 'Identitas Pasien',
                            text: "Data booking tidak ditemukan",
                            footer: `Jika ada pertanyaan silahkan hubungi 0812-2517-6300`,
                            showConfirmButton: false,
                        })
                    }

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('#btn_form_cek').text("Cek Identitas")
                    $('#btn_form_cek').prop("disabled", false)
                    if(jqXHR.status == 422){
                        let error = jqXHR.responseJSON?.data || []
                        let errorMessage = ''
                        for (const property in error) {
                            errorMessage += `${error[property]}, `;
                        }
                        Swal.fire({
                            icon: 'error',
                            title: 'Form Validasi',
                            text: errorMessage,
                            showConfirmButton: false,
                            footer: `${jqXHR.status} - ${jqXHR.statusText}`
                        })
                        return false;
                    }
                    toastr.error(jqXHR.responseJSON?.metadata?.message || jqXHR.statusText)
                },
            });


        })

    })(jQuery)
    </script>
@endsection

