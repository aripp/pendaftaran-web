@extends('layouts.app')
@section('title', $title)

@section('content')
    <div class="container-lg">
        <div class="row">
            <div class="col-md-12 py-2">
                <div class="card">
                    <div class="card-header">
                        <a href="{{ route('dashboard') }}" class="text-decoration-none text-reset"><i class="fas fa-arrow-left mr-1"></i></a>
                        Pendaftaran berhasil
                    </div>
                    <div class="card-body text-center">
                        <img src="{{ asset('image/complete.png') }}" alt="pendaftaran-online-rs-islam-boyolali" class="img-fluid" style="height: 15rem">
                        <h3>Yeey, pendaftaran berhasil 🥳</h3>
                        <p class="text-muted">Jika bukti booking tidak terdownload otomatis, silahkan download manual dengan klik tombol dibawah ini</p>
                         <button class="btn btn-danger" id="btn_download">Download Bukti Booking</button>
                        <iframe id="dwn_frame" src="" height="0" width="0" class="d-none"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section("javascript")
    <script type="text/javascript">
    const formPendaftaranBaru = (function($) {
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        const id = encodeURIComponent(urlParams.get('id'))

        $(document).ready(function() {
            $('#dwn_frame').attr('src', `${window.origin}/api/bukti-booking?id=${id}`);
        })
        $("#btn_download").click( e => {
            window.location.replace(window.origin + '/api/bukti-booking?id=' + id);
        })
    })(jQuery)

    </script>
@endsection

