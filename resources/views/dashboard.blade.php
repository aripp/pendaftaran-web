@extends('layouts.app')
@section('title', $title)

@section('content')
    <div class="container">
        <div class="row">
            {{-- <div class="col-md-12 py-2">
                <nav>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">Menu</li>
                    </ol>
                </nav>
            </div> --}}
            <div class="col-md-4 py-2 d-none">
                <a href="{{ route('reg.baru') }}" class="text-decoration-none text-reset">
                    <div class="card shadow-lg">
                        <img class="card-img-top d-none d-md-block border-bottom" src="{{ asset('image/reg-pasien-baru.png') }}" alt="RSU ISLAM BOYOLALI" style="height: 10rem; object-fit: contain;">
                        <div class="card-body">
                            <p class="card-title">Pendaftaran pasien baru</p>
                            <p class="card-text text-muted">Untuk booking jadwal dokter pasien yang belum pernah periksa</p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-4 py-2">
                <a href="{{ route('reg.lama') }}" class="text-decoration-none text-reset">
                    <div class="card shadow-lg">
                        <img class="card-img-top d-none d-md-block border-bottom" src="{{ asset('image/reg-pasien-lama.png') }}" alt="RSU ISLAM BOYOLALI" style="height: 10rem; object-fit: contain;">
                        <div class="card-body">
                            <p class="card-title">Pendaftaran pasien lama</p>
                            <p class="card-text text-muted">Untuk booking jadwal dokter pasien yang sudah pernah periksa</p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-4 py-2">
                <a href="{{ route('antrean') }}" class="text-decoration-none text-reset">
                    <div class="card shadow-lg">
                        <img class="card-img-top d-none d-md-block border-bottom" src="{{ asset('image/antrian.png') }}" alt="RSU ISLAM BOYOLALI" style="height: 10rem; object-fit: contain;">
                        <div class="card-body">
                            <p class="card-title">Antrian</p>
                            <p class="card-text text-muted">Informasi mengenai antrian dokter</p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-4 py-2">
                <a href="{{ route('jadwal') }}" class="text-decoration-none text-reset">
                    <div class="card shadow-lg">
                        <img class="card-img-top d-none d-md-block border-bottom" src="{{ asset('image/time.png') }}" alt="RSU ISLAM BOYOLALI" style="height: 10rem; object-fit: contain;">
                        <div class="card-body">
                            <p class="card-title">Jadwal dokter</p>
                            <p class="card-text text-muted">Informasi mengenai jadwal dokter</p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-4 py-2">
                <a href="{{ route('riw.booking.identitas') }}" class="text-decoration-none text-reset">
                    <div class="card shadow-lg">
                        <img class="card-img-top d-none d-md-block border-bottom" src="{{ asset('image/booking-info.png') }}" alt="RSU ISLAM BOYOLALI" style="height: 10rem; object-fit: contain;">
                        <div class="card-body">
                            <p class="card-title">Booking Info</p>
                            <p class="card-text text-muted">Informasi mengenai riwayat booking</p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-4 py-2">
                <a href="{{ route('kamar') }}" class="text-decoration-none text-reset">
                    <div class="card shadow-lg">
                        <img class="card-img-top d-none d-md-block border-bottom" src="{{ asset('image/bed.png') }}" alt="RSU ISLAM BOYOLALI" style="height: 10rem; object-fit: contain;">
                        <div class="card-body">
                            <p class="card-title">Kamar</p>
                            <p class="card-text text-muted">Informasi mengenai ketersediaan kamar</p>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
@endsection

