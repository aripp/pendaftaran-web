@extends('layouts.app')
@section('title', $title)
@section('head')
    {!! NoCaptcha::renderJs() !!}
@endsection


@section('content')
    <div class="container-lg">
        <form id="formulir" class="row" autocomplete="off">
            <div class="col-md-12 py-2">
                <nav>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}" class="text-decoration-none text-reset"><i class="fas fa-arrow-left mr-1"></i>Menu</a></li>
                        <li class="breadcrumb-item active">Pendaftaran pasien baru</li>
                    </ol>
                </nav>
            </div>
            <div class="col-md-4 py-2">
                <div class="card shadow">
                    <div class="card-header">
                        Data pasien
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label class="form-label">Nama</label>
                            <input type="text" class="form-control" id="nama" name="nama" >
                        </div>
                        <div class="form-group">
                            <label class="form-label">No KTP/NIK</label>
                            <input type="text" class="form-control" id="no_ktp" name="no_ktp">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Jenis kelamin</label>
                            <select class="form-control my-select2-nc-nc" id="jenis_kelamin" name="jenis_kelamin">
                                <option value=""></option>
                                <option value="L">Laki laki</option>
                                <option value="P">Perempuan</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Tempat, tanggal lahir</label>
                            <div class="input-group input-group">
                                <input type="text" class="form-control w-50" id="tmp_lahir" name="tmp_lahir">
                                <input type="text" class="form-control w-50" id="tgl_lahir" name="tgl_lahir" placeholder="DD-MM-YYYY">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Agama </label>
                            <select class="form-control my-select2-nc" id="agama" name="agama">
                                <option value=""></option>
                                @foreach ($masterAgama as $item)
                                <option value="{{ $item->agama }}">{{ $item->agama}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Etnis/Suku </label>
                            <select class="form-control my-select2-nc" id="etnis" name="etnis">
                                <option value=""></option>
                                @foreach ($masterEtnis as $item)
                                <option value="{{ $item->etnis }}">{{ $item->etnis}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Bahasa </label>
                            <select class="form-control my-select2-nc" id="bahasa" name="bahasa">
                                <option value=""></option>
                                @foreach ($masterBahasa as $item)
                                <option value="{{ $item->bahasa }}">{{ $item->bahasa}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Pendidikan </label>
                            <select class="form-control my-select2-nc" id="pendidikan" name="pendidikan">
                                <option value=""></option>
                                @foreach ($masterPendidikan as $item)
                                <option value="{{ $item->pendidikan }}">{{ $item->pendidikan}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Pekerjaan </label>
                            <select class="form-control my-select2-nc" id="pekerjaan" name="pekerjaan">
                                <option value=""></option>
                                @foreach ($masterPekerjaan as $item)
                                <option value="{{ $item->pekerjaan }}">{{ $item->pekerjaan}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Perkawinan</label>
                            <select class="form-control my-select2-nc" id="status_perkawinan" name="status_perkawinan">
                                <option value=""></option>
                                @foreach ($masterPerkawinan as $item)
                                <option value="{{ $item->status_kawin }}">{{ $item->status_kawin}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Nama ibu </label>
                            <input type="text" class="form-control" id="nama_ibu" name="nama_ibu">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Nama ayah </label>
                            <input type="text" class="form-control" id="nama_ayah" name="nama_ayah">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Nama pasangan</label>
                            <input type="text" class="form-control" id="nama_suami_istri" name="nama_suami_istri">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 py-2">
                <div class="card shadow">
                    <div class="card-header">
                        Data Alamat
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label class="form-label">Alamat kabupaten</label>
                            <select class="form-control my-select2-nc" id="kota_kd" name="kota_kd">
                                <option value=""></option>
                                @foreach ($masterKabupaten as $item)
                                <option value="{{ $item->id }}">{{ $item->nama}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Alamat Kecamatan</label>
                            <div class="spinner-border text-success" id="kecamatan_loading"><span class="sr-only">Loading...</span></div>
                            <select class="form-control" id="kecamatan_kd" name="kecamatan_kd">
                                <option value=""></option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Alamat Lengkap</label>
                            <textarea class="form-control" id="alamat" name="alamat"></textarea>
                        </div>
                        <div class="form-group">
                            <label class="form-label">No Telp/HP </label>
                            <input type="text" class="form-control" id="no_telp" name="no_telp">
                        </div>
                        <div class="form-group">
                            <label class="form-label">No Whatsapp </label>
                            <input type="text" class="form-control" id="no_wa" name="no_wa">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Email</label>
                            <input type="text" class="form-control" id="email" name="email">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 py-2">
                <div class="card shadow">
                    <div class="card-header">
                        Jadwal dan Cara bayar
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label class="form-label">Cara bayar/Asuransi</label>
                            <select class="form-control my-select2-nc" id="asuransi_id" name="asuransi_id">
                                <option value=""></option>
                                @foreach ($masterAsuransi as $item)
                                @if ($item->asuransi_nama == "Umum")
                                <option value="{{ $item->asuransi_id }}">{{ $item->asuransi_nama}} / Bayar Sendiri</option>
                                @else
                                <option value="{{ $item->asuransi_id }}">{{ $item->asuransi_nama}}</option>
                                @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group" id="wrap_no_bpjs">
                            <label class="form-label">No BPJS</label>
                            <input type="tel" class="form-control" id="no_bpjs" name="no_bpjs">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Tanggal Periksa</label>
                            <input type="text" class="form-control datetimepicker-input" id="tgl_masuk" name="tgl_masuk" data-toggle="datetimepicker">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Jadwal</label>
                            <div class="spinner-border text-success" id="jadwal_loading"><span class="sr-only">Loading...</span></div>
                            <select class="form-control" id="jadwal_id" name="jadwal_id">
                                <option value=""></option>
                            </select>
                        </div>
                        <div id="jadwal_detail"></div>
                    </div>
                </div>
                <div class="card shadow mt-3">
                    <div class="card-body">
                            {!! NoCaptcha::display() !!}
                    </div>
                    <div class="card-footer">
                        <div class="text-center">
                            <button class="btn btn-dark shadow-lg" id="btn_form_daftar">Daftar Sekarang</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

<!-- Modal -->
<div class="modal fade" id="mdl_ketentuan" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Kebijakan & Ketentuan</h5>
      </div>
      <div class="modal-body">
        <ol>
            <li>Setuju dan memberi izin kepada dokter yang bersangkutan untuk merawat, mengobati sesuai dengan prosedur diagnostik yang berlaku di RSU Islam Boyolali.</li>
            <li>Setuju dengan tindakan-tindakan yang lazim dilakukan di RSU Islam Boyolali, seperti pemasangan infus, Selang lambung/NGT, selang kateter, injeksi dan medikasi.</li>
            <li>Sanggup/bersedia membayar seluruh biaya perawatan, pengobatan dan biaya administrasi.</li>
            <li>Memberi kuasa kepada dokter/pihak RSU Islam Boyolali untuk memberikan keterangan secukupnya yang diperlukan oleh penanggung biaya perawatan saya/pasien tersebut diatas.</li>
            <li>Saya menyatakan bahwa saya telah menerima informasi dan mengetahui tentang adanya tata cara mengajukan keluhan dengan cara SMS ke No. +62 813-9269-8124, Email: humas_rsuiboyolali@gmail.com atau bisa menulis langsung dan diserahkan kepada petugas RS.</li>
        </ol>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-secondary" id="btn_ketentuan_tidak_setuju">Tidak setuju</button>
        <button type="button" class="btn btn-dark" id="btn_ketentuan_setuju">Setuju</button>
      </div>
    </div>
  </div>
</div>

@endsection
@section("javascript")
    <script type="text/javascript">
    const formPendaftaranBaru = (function($) {
        const ASURANSI_BPJS = [2, 3, 4]
        const TANGGAL_NOW = moment().startOf("day");
        const TANGGAL_MIN_INDEN = moment().startOf("day").add(1, 'days');
        const TANGGAL_MAX_INDEN = moment().startOf("day").add(1, 'months');

        $(document).ready(function() {
            // $("#mdl_ketentuan").modal('show')
            $("#jadwal_loading").addClass("d-none")
            $("#kecamatan_loading").addClass("d-none")
            $("#jadwal_detail").html("")
            $('#tgl_lahir').val('')
            $('#wrap_no_bpjs').addClass('d-none')
            getJadwal(TANGGAL_MIN_INDEN)
            $('#btn_form_daftar').text("Daftar Sekarang")
            $('#btn_form_daftar').prop("disabled", false)
        })

        $('#jenis_kelamin').select2({
            placeholder: '',
            allowClear: true,
            theme: 'bootstrap4',
        });
        $('#tgl_lahir').inputmask('99-99-9999');
        $('#no_ktp').inputmask('9999999999999999');
        $('#no_bpjs').inputmask('9999999999999');

        $("#kota_kd").change( e => {
            let kabupaten = $("#kota_kd").val()
            getKecamatan(kabupaten)
        })
        function getKecamatan(kabupaten){
            $("#kecamatan_kd option").remove()
            $("#kecamatan_kd").html("<option values=''></option>")
            if(!kabupaten) return false

            $.ajax({
                method: 'GET',
                dataType: 'JSON',
                url: window.origin + '/api/ref/kecamatan/kabupaten/' + kabupaten ,
                beforeSend(xhr) {
                    $("#kecamatan_loading").removeClass("d-none")
                },
                success: function (result) {
                    $("#kecamatan_loading").addClass("d-none")

                    let data = result.data.master_kecamatan.map( e => ({id: e.id, text: e.nama}))
                    $('#kecamatan_kd').select2({
                        placeholder: '',
                        allowClear: true,
                        theme: 'bootstrap4',
                        data:data
                    });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $("#kecamatan_loading").addClass("d-none")
                    toastr.error(jqXHR.responseJSON?.metadata?.message || jqXHR.statusText)
                },
            });
        }
        $('#tgl_masuk').datetimepicker({
            format: 'dddd, DD-MM-YYYY',
            maxDate: TANGGAL_MAX_INDEN,
            minDate: TANGGAL_MIN_INDEN
        })
        $('#asuransi_id').change(e => {
            let asuransi = parseInt($('#asuransi_id').val())
            $('#no_bpjs').val('')
            if(ASURANSI_BPJS.includes(asuransi)){
                $('#wrap_no_bpjs').removeClass('d-none')
            }else{
                $('#wrap_no_bpjs').addClass('d-none')
            }
        })
        $('#tgl_masuk').on('hide.datetimepicker', function (e) {
            let tanggal = $('#tgl_masuk').val()
            getJadwal(tanggal)
        })
        function getJadwal(tanggal){
            $("#jadwal_detail").html("")
            $("#jadwal_id option").remove()
            $("#jadwal_id").html("<option values=''></option>")
            if(!tanggal) return

            $.ajax({
                method: 'GET',
                dataType: 'JSON',
                url: window.origin + '/api/jadwal/show-by-tanggal/' +  moment(tanggal, "dddd, DD-MM-YYYY").format("YYYY-MM-DD"),
                beforeSend(xhr) {
                    $("#jadwal_loading").removeClass("d-none")
                },
                success: function (result) {
                    $("#jadwal_loading").addClass("d-none")
                    let data = []
                    if(result.data.jadwal.length > 0){
                        data = result.data.jadwal.map( e => ({
                            id: e.id,
                            text: `${e.dokter_nama} <span class="badge badge-success"> ${e.poli_nama} ${e.jam_mulai} - ${e.jam_selesai} (${e.waktu})</span> `
                        }))
                    }
                    $('#jadwal_id').select2({
                        placeholder: '',
                        allowClear: true,
                        theme: 'bootstrap4',
                        data:data,
                        escapeMarkup: (markup) => markup,
                        templateResult: (data) => data.text,
                        templateSelection: (data) => data.text
                    });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $("#jadwal_loading").addClass("d-none")
                    toastr.error(jqXHR.responseJSON?.metadata?.message || jqXHR.statusText)
                },
            });
        }
        $("#jadwal_id").change( e => {
            let jadwal_id = $("#jadwal_id").val()
            let tanggal = $('#tgl_masuk').val()

            getJadwalDetail(jadwal_id, tanggal)
        })
        function getJadwalDetail(id, tanggal){
            $("#jadwal_detail").html("")
            if(!id) return

            $.ajax({
                method: 'GET',
                dataType: 'JSON',
                url: window.origin + '/api/jadwal/show-by-id/' +  id +'/tanggal/' + moment(tanggal, "dddd, DD-MM-YYYY").format("YYYY-MM-DD"),
                beforeSend(xhr) {
                    $("#jadwal_detail").html(`<div class="spinner-border text-success"><span class="sr-only">Loading...</span></div>`)
                },
                success: function (result) {
                    let html = ""
                    if(result.data.jadwal){
                        html = `
                                        <table class="table table-sm">
                                            <thead>
                                                <tr>
                                                    <th colspan=2>Detail Jadwal</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="w-25">Dokter</td>
                                                    <td class="w-75">${result.data.jadwal.dokter_nama}</td>
                                                </tr>
                                                <tr>
                                                    <td class="w-25">Poli</td>
                                                    <td class="w-75">${result.data.jadwal.poli_nama}</td>
                                                </tr>
                                                <tr>
                                                    <td class="w-25">Waktu</td>
                                                    <td class="w-75">${moment(result.data.jadwal.tanggal, "YYYY-MM-DD").format("dddd, DD-MM-YYYY")} <br/>${result.data.jadwal.jam_mulai} sd ${result.data.jadwal.jam_selesai} </td>
                                                </tr>
                                                <tr>
                                                    <td class="w-25">Kuota</td>
                                                    <td class="w-75">${result.data.jadwal.sisa_app}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                        `
                    }else{
                        html = ""
                    }
                    $("#jadwal_detail").html(html)
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $("#jadwal_detail").html("")
                    toastr.error(jqXHR.responseJSON?.metadata?.message || jqXHR.statusText)
                },
            });
        }
        $('#btn_form_daftar').click( e => {
            e.preventDefault()

            let form =  $("#formulir").serialize();
            form += '&tgl_masuk_only_date=' + moment($("#tgl_masuk").val(),'dddd, DD-MM-YYYY').format("DD-MM-YYYY")

            $.ajax({
                method: 'POST',
                dataType: 'JSON',
                data: form,
                url: window.origin + '/api/pendaftaran-save',
                beforeSend(xhr) {
                    $('#btn_form_daftar').text("Mendaftar ...")
                    $('#btn_form_daftar').prop("disabled", true)
                },
                success: function (result) {
                    $('#btn_form_daftar').text("Daftar Sekarang")
                    $('#btn_form_daftar').prop("disabled", false)

                    let id = result.data.id

                    Swal.fire({
                        icon:'success',
                        html:`<h5 class="">${result.metadata.message}</h5>`,
                        width: 530,
                        showConfirmButton: false,
                        allowEscapeKey: false,
                        allowOutsideClick: false,
                        timer: 1000
                    }).then(() => {
                       window.location.replace(window.origin + '/pendaftaran-berhasil?id=' + id);
                    })
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('#btn_form_daftar').text("Daftar Sekarang")
                    $('#btn_form_daftar').prop("disabled", false)
                    if(jqXHR.status == 422){
                        let error = jqXHR.responseJSON?.data || []
                        let errorMessage = ''
                        for (const property in error) {
                            errorMessage += `${error[property]}, `;
                        }
                        Swal.fire({
                            icon: 'error',
                            title: 'Form Validasi',
                            text: errorMessage,
                            showConfirmButton: false,
                            footer: `${jqXHR.status} - ${jqXHR.statusText}`
                        })
                        return false;
                    }
                    toastr.error(jqXHR.responseJSON?.metadata?.message || jqXHR.statusText)
                },
            });


        })
        $("#btn_ketentuan_tidak_setuju").click( e => {
            window.location.replace(window.origin);
        })
        $("#btn_ketentuan_setuju").click( e => {
            $("#mdl_ketentuan").modal('hide')
        })
    })(jQuery)

    </script>
@endsection

